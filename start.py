#!/usr/bin/env python3
import pystoki

if __name__ == '__main__':
    import sys
    pystoki.start(*sys.argv[1:])


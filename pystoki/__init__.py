import logging

from .utils import logging_init
# from .labs import Post2

__author__ = 'feo'


def start(post):
    logging_init()
    logging.info('Starting: {}'.format(post))
    if post == 'post2':
        from .labs.post2 import Post2
        cur_lab = Post2()
    elif post == 'post4':
        from .labs.post4 import Post4
        cur_lab = Post4()
    else:
        logging.error('Invalid argument {}'.format(post))
        return 1

    cur_lab.run()

from datetime import datetime, timedelta

import os
import os.path

from ..utils import ARCHIVE_DIR


class ArchiveDatabase(object):
    base_path = ''
    record_date_format = ''
    record_format = ''
    file_name_format = ''
    date_file_format = ''
    days_in_file = 1

    def __init__(self):
        self.create_paths()

    def add(self, msg):
        file_name = self.get_file_name(msg)
        with open(file_name, 'a') as f:
            f.write(self.record_format.format(msg) + '\n')

    def create_paths(self):
        if not os.path.exists(self.base_path):
            os.mkdir(self.base_path)

    def get_file_name(self, msg):
        file_name = self.file_name_format.format(msg)
        return os.path.join(self.base_path, file_name)

    def read(self, date_start, date_end, code=''):
        from ..messages import Message
        files = self.get_file_list(date_start, date_end, code)
        result = []
        for file_name, f_code in files:
            with open(file_name) as f:
                for line in f.readlines():
                    line = line.strip()
                    try:
                        msg = self.parse_line(line, f_code)
                        if date_start <= msg['date'] < date_end:
                            result.append(Message(kwargs=msg, save=False))
                    except ValueError:
                        pass
        return result

    def get_file_list(self, date_start, date_end, code):
        res = []
        ld = os.listdir(self.base_path)
        ld.sort()
        for file_name in ld:
            check, f_code = self.check_file_name(file_name, date_start, date_end, code)
            if check:
                res.append((os.path.join(self.base_path, file_name), f_code))
        return res

    def check_file_name(self, file_name, date_start, date_end, code):
        if not file_name.startswith(code):
            return False, ''
        f_code = file_name.rstrip('0123456789')
        file_name = file_name[len(code):]
        if file_name == '' or not file_name[0].isnumeric():
            return False, f_code
        file_date_start = datetime.strptime(file_name, self.date_file_format)
        file_date_end = file_date_start + timedelta(days=self.days_in_file)
        if date_start <= file_date_end and date_end >= file_date_start:
            return True, f_code
        return False, f_code

    def parse_line(self, line, code):
        raise NotImplementedError()


class CurrentArchiveDatabase(ArchiveDatabase):
    base_path = os.path.join(ARCHIVE_DIR, 'current')
    file_name_format = '{0.code}{0.date:%Y%m%d}'
    record_format = '{0.date:%d.%m.%Y %H:%M:%S}\t{0.value:.2f}\t{0.quality}'
    date_file_format = '%Y%m%d'

    def parse_line(self, line, code):
        dt, val, quality = line.split('\t')
        return {
            'date': datetime.strptime(dt, '%d.%m.%Y %H:%M:%S'),
            'value': val,
            'quality': quality,
            'type': 'current',
            'code': code,
        }


class MessageArchiveDatabase(ArchiveDatabase):
    base_path = os.path.join(ARCHIVE_DIR, 'message')
    file_name_format = '{0.date:%Y%m%d}'
    record_format = '{0.date:%d.%m.%Y %H:%M:%S}\t{0.code}\t{0.value:.2f}'
    date_file_format = '%Y%m%d'

    def parse_line(self, line, code):
        dt, c, val = line.split('\t')
        return {
            'date': datetime.strptime(dt, '%d.%m.%Y %H:%M:%S'),
            'value': val,
            'quality': 0,
            'type': 'message',
            'code': c,
        }


class HourlyArchiveDatabase(ArchiveDatabase):
    base_path = os.path.join(ARCHIVE_DIR, 'hourly')
    file_name_format = '{0.code}{0.date:%Y%m%d}'
    record_format = '{0.date:%d.%m.%Y %H:%M:%S}\t{0.value:.2f}'
    date_file_format = '%Y%m%d'

    def parse_line(self, line, code):
        dt, val = line.split('\t')
        return {
            'date': datetime.strptime(dt, '%d.%m.%Y %H:%M:%S'),
            'value': val,
            'quality': 0,
            'type': 'archive_hourly',
            'code': code,
        }


class DailyArchiveDatabase(ArchiveDatabase):
    base_path = os.path.join(ARCHIVE_DIR, 'daily')
    file_name_format = '{0.code}{0.date:%Y%m}'
    record_format = '{0.date:%d.%m.%Y}\t{0.value:.2f}'
    date_file_format = '%Y%m'
    days_in_file = 31

    def parse_line(self, line, code):
        dt, val = line.split('\t')
        return {
            'date': datetime.strptime(dt, '%d.%m.%Y'),
            'value': val,
            'quality': 0,
            'type': 'archive_dayly',
            'code': code,
        }


class Archive(object):

    def __init__(self):
        self.databases = {
            'current': CurrentArchiveDatabase(),
            'message': MessageArchiveDatabase(),
            'archive_hourly': HourlyArchiveDatabase(),
            'archive_dayly': DailyArchiveDatabase(),
        }

    def add(self, msg):
        self.databases[msg.type].add(msg)

    def read(self, date_start, date_end, type_, code=''):
        db = self.databases[type_]
        return db.read(date_start, date_end, code)

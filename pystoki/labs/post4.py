from functools import partial

from ..hw import IRKA, SC1000, I7000, Sibold, SK500
from ..lab import Laboratory
from ..tags import Tag
from ..task import Promivka, CheckPower, CalculateHoursValues, CalculateDailyValues, CheckPump


class Post4(Laboratory):
    def __init__(self):
        super().__init__("post4")
        self.devices = {
            'irka': IRKA(code='irka'),
            'sc1000': SC1000(code='sc1000'),
            'i7000_1': I7000(code='i7000_1'),
            'i7000_2': I7000(code='i7000_2'),
            'i7000_3': I7000(code='i7000_3'),
            'sibold': Sibold(code='sibold'),
            'sk500': SK500(code='sk500'),
        }
        self.freq_reg = self.devices['sk500']

        self.vent_lab_ctl = partial(self.devices['i7000_1'].set_output, 0)
        self.vent_gen_ctl = partial(self.devices['i7000_1'].set_output, 1)
        self.pump_pow_ctl = partial(self.devices['i7000_1'].set_output, 2)
        self.barb_ctl = partial(self.devices['i7000_3'].set_output, 1)
        self.sink_ctl = partial(self.devices['i7000_3'].set_output, 0)

        self.tags = {
            'ftot': Tag(
                code='ftot',
                fn=self.devices['irka'].read_total,
                ),
            'f': Tag(
                code='f',
                fn=self.devices['irka'].read_flow,
                ),
            'ph': Tag(
                code='ph',
                fn=self.devices['sc1000'].read_ph,
                ),
            't': Tag(
                code='t',
                fn=self.devices['sc1000'].read_t,
                ),
            'vv': Tag(
                code='vv',
                fn=self.devices['sc1000'].read_vv,
                ),
            'oil': Tag(
                code='oil',
                fn=self.devices['sc1000'].read_oil,
                ),
            'fe': Tag(
                code='fe',
                fn=self.devices['sibold'].read_fe,
                ),
            'cl': Tag(
                code='cl',
                fn=self.devices['sibold'].read_cl,
                ),
            'power': Tag(
                code='power',
                fn=partial(self.devices['i7000_3'].get_input, 2),
                on_change=True,
                ),
            'gen': Tag(
                code='gen',
                fn=partial(self.devices['i7000_3'].get_input, 3),
                on_change=True,
                ),
            'vent_lab': Tag(
                code='vent_lab',
                fn=partial(self.devices['i7000_1'].get_input, 0),
                on_change=True,
                ),
            'vent_gen': Tag(
                code='vent_gen',
                fn=partial(self.devices['i7000_1'].get_input, 1),
                on_change=True,
                ),
            'pump_pow': Tag(
                code='pump_pow',
                fn=partial(self.devices['i7000_1'].get_input, 2),
                on_change=True,
                ),
        }

        self.tasks = [
            ('prom', Promivka(code='prom', lab=self)),
            ('check', CheckPower(code='check', lab=self)),
            ('check_pump', CheckPump(code='check_pump', lab=self)),
            ('hourly', CalculateHoursValues(code='hours_calc', lab=self)),
            ('daily', CalculateDailyValues(code='daily_calc', lab=self)),
        ]

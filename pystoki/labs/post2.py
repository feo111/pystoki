from functools import partial
import logging

from ..hw import SC1000, I7000, I7061, Vega, IQ, Micromac, ProtoError, Device, TwoStateDevice, ZelioDevice
from ..lab import Laboratory
from ..tags import Tag, FilteredTag
from ..task import Calibrate, CalculateHoursValues, CalculateDailyValues
from ..task import CheckPump, ProdIQStates, Promivka2States, CheckPower, ValveSelect


# noinspection PyBroadException
class Post2(Laboratory):
    def __init__(self):
        super().__init__("post2")
        self.devices = {
            'sc1000': SC1000(code='sc1000'),
            'i7000_1': I7061(code='i7000_1'),
            'i7000_2': I7000(code='i7000_2'),
            'i7000_3': I7000(code='i7000_3'),
            'vega': Vega(code='vega'),
            'iq': IQ(code='iq'),
        }
        self.freq_reg = self.devices['vega']

        self.barb1_ctl = TwoStateDevice('barb1_ctl', self.devices['i7000_1'], 1)
        self.barb2_ctl = TwoStateDevice('barb2_ctl', self.devices['i7000_1'], 2)
        self.barbIQ_ctl = TwoStateDevice('barbIQ_ctl', self.devices['i7000_1'], 3)
        self.barb3_ctl = TwoStateDevice('barb3_ctl', self.devices['i7000_1'], 4)
        # self.devices['zelio'] = ZelioDevice('zelio', self.devices['i7000_1'], 5)
        self.clap_3hod = TwoStateDevice('clap_3hod', self.devices['i7000_1'], 6)
        self.valve_mid_ctl = TwoStateDevice('valve_mid_ctl', self.devices['i7000_1'], 8)
        self.valve_hi_ctl = TwoStateDevice('valve_hi_ctl', self.devices['i7000_1'], 9)
        self.valve_lo_ctl = TwoStateDevice('valve_lo_ctl', self.devices['i7000_1'], 10)

        self.pump_pow_ctl = TwoStateDevice('pump_pow_ctl', self.devices['i7000_2'], 0)
        self.sink_ctl = TwoStateDevice('sink_ctl', self.devices['i7000_2'], 1)
        self.pump_probe_ctl = TwoStateDevice('pump_probe_ctl', self.devices['i7000_2'], 3)

        self.vent_lab_ctl = TwoStateDevice('vent_lab_ctl', self.devices['i7000_3'], 0)
        self.vent_gen_ctl = TwoStateDevice('vent_gen_ctl', self.devices['i7000_3'], 1)

        # self.devices['barb1_ctl'] = self.barb1_ctl
        # self.devices['barb2_ctl'] = self.barb2_ctl
        # self.devices['barbIQ_ctl'] = self.barbIQ_ctl
        # self.devices['barb3_ctl'] = self.barb3_ctl
        # self.devices['clap_3hod'] = self.clap_3hod
        # self.devices['valve_mid_ctl'] = self.valve_mid_ctl
        # self.devices['valve_hi_ctl'] = self.valve_hi_ctl
        # self.devices['valve_lo_ctl'] = self.valve_lo_ctl
        # self.devices['pump_pow_ctl'] = self.pump_pow_ctl
        # self.devices['sink_ctl'] = self.sink_ctl
        # self.devices['pump_probe_ctl'] = self.pump_probe_ctl
        # self.devices['vent_lab_ctl'] = self.vent_lab_ctl
        # self.devices['vent_gen_ctl'] = self.vent_gen_ctl

        self.devices['micromac_fe'] = Micromac('micromac_fe', self.pump_probe_ctl)
        self.devices['micromac_cl'] = Micromac('micromac_cl', self.pump_probe_ctl)

        self.tags = {
            'f': FilteredTag(
                code='f',
                fn=self.devices['iq'].read_flow,
            ),
            'ftot': Tag(
                code='ftot',
                fn=self.devices['iq'].read_total,
            ),
            'flev': Tag(
                code='flev',
                fn=self.devices['iq'].read_level,
            ),
            'ftemp': Tag(
                code='ftemp',
                fn=self.devices['iq'].read_temp,
            ),
            'ph': Tag(
                code='ph',
                fn=self.devices['sc1000'].read_ph,
            ),
            't': Tag(
                code='t',
                fn=self.devices['sc1000'].read_t,
            ),
            'vv': Tag(
                code='vv',
                fn=self.devices['sc1000'].read_vv,
            ),
            'oil': Tag(
                code='oil',
                fn=self.devices['sc1000'].read_oil,
            ),
            'fe': Tag(
                code='fe',
                fn=self.devices['micromac_fe'].read_value,
            ),
            'cl': Tag(
                code='cl',
                fn=self.devices['micromac_cl'].read_value,
            ),

            'pump_pow': Tag(
                code='pump_pow',
                fn=partial(self.devices['i7000_2'].get_input, 0),
                on_change=True,
            ),
            'sliv': Tag(
                code='sliv',
                fn=partial(self.devices['i7000_2'].get_input, 1),
                on_change=True,
            ),
            'power': Tag(
                code='power',
                fn=partial(self.devices['i7000_2'].get_input, 2),
                on_change=True,
            ),
            'gen': Tag(
                code='gen',
                fn=partial(self.devices['i7000_2'].get_input, 3),
                on_change=True,
            ),
            'vent_lab': Tag(
                code='vent_lab',
                fn=partial(self.devices['i7000_3'].get_input, 0),
                on_change=True,
            ),
            'vent_gen': Tag(
                code='vent_gen',
                fn=partial(self.devices['i7000_3'].get_input, 1),
                on_change=True,
            ),
            'clap3_state': Tag(
                code='clap3_state',
                fn=partial(self.devices['i7000_3'].get_input, 2),
                on_change=True,
            ),
            'pump_probe_state': Tag(
                code='pump_probe_state',
                fn=partial(self.devices['i7000_3'].get_input, 3),
                on_change=True,
            ),

        }

        self.tasks = [
            # ('prom', Promivka2(code='prom', lab=self)),
            ('valveselect', ValveSelect(code='valveselect', lab=self)),
            ('prom', Promivka2States(code='prom', lab=self)),
            ('prodiq', ProdIQStates(code='prodiq', lab=self)),
            # ('checkpump', CheckPump(code='checkpump', lab=self)),
            ('checkpow', CheckPower(code='checkpow', lab=self)),
            # ('calibrate_fe', Calibrate(code='calibrate_fe', lab=self, device=self.devices['micromac_fe'])),
            # ('calibrate_cl', Calibrate(code='calibrate_cl', lab=self, device=self.devices['micromac_cl'])),
            ('hourly', CalculateHoursValues(code='hours_calc', lab=self)),
            ('daily', CalculateDailyValues(code='daily_calc', lab=self)),
        ]
        self.check_bus()

    def check_bus(self):
        logging.info('Checking ports...')
        ports = {'/dev/ttyUSB0', '/dev/ttyUSB1'}
        i7000_2 = self.devices['i7000_2']
        ok = False
        logging.info('Search i7000_2...')
        port = ''
        for port in ports:
            try:
                logging.info(' ...on port ' + port)
                i7000_2.port = port
                s = i7000_2.get_input(0)
                ok = s is not None
                break
            except Exception:
                pass
        if ok:
            logging.info('I7000_2 is on ' + port)
            self.devices['vega'].port = port
            self.devices['sc1000'].port = port
            self.devices['i7000_1'].port = port
            self.devices['i7000_2'].port = port
            self.devices['i7000_3'].port = port
            # self.devices['micromac_fe'].port = port
            ports.remove(port)
        else:
            logging.info('I7000_2 not found')

        # ok = False
        # micromac_cl = self.devices['micromac_cl']
        # logging.info('Search micromac_cl...')
        # for port in ports:
        #     try:
        #         logging.info(' ...on port ' + port)
        #         micromac_cl.port = port
        #         s = micromac_cl.cur_status
        #         ok = True
        #         break
        #     except ProtoError:
        #         pass
        # if ok:
        #     logging.info('Micromac_cl is on ' + port)
        #     ports.remove(port)
        # else:
        #     logging.info('Micromac_cl not found')

        ok = False
        iq = self.devices['iq']
        logging.info('Search iq...')
        for port in ports:
            try:
                logging.info(' ...on port ' + port)
                iq.port = port
                s = iq.read_flow()
                ok = s is not None
                break
            except Exception:
                pass
        if ok:
            logging.info('IQ is on ' + port)
            ports.remove(port)
        else:
            logging.info('IQ not found')

    def cam_filled(self):
        vv, st = self.devices['sc1000'].read_vv()
        if st != Device.STATUS_OK:
            return True
        return vv < 100

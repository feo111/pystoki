import logging
import sys
import struct
from datetime import datetime

import minimalmodbus
import serial.rs485

from .device import Device, NoRespondError, RespondError, ProtoError
from ..lab import Laboratory
from ..utils import TempState, repeatedly, Configurable


class MicromacProto(minimalmodbus.Instrument):
    STATE_STANDBY = 1
    STATE_ALARM = 2
    STATE_ANALYSIS = 3
    STATE_CALIBRATION = 4
    STATE_BLANK = 5
    STATE_WASH = 6
    STATE_FILL = 7
    STATE_CLEAN = 8

    def __init__(self, port, address):
        super().__init__(port, address)
        self.close_port_after_each_call = True
        self.serial.close()
        self.serial = serial.rs485.RS485(port,
                                         baudrate=self.serial.baudrate,
                                         timeout=self.serial.timeout,
                                         )
        self.serial.close()
        self.serial.rs485_mode = serial.rs485.RS485Settings()

    # noinspection PyPep8Naming
    def read_float(self, registeraddress, functioncode=3, numberOfRegisters=2):
        try:
            return super().read_float(registeraddress, functioncode, numberOfRegisters)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    # noinspection PyPep8Naming
    def read_register(self, registeraddress, numberOfDecimals=0, functioncode=3, signed=False):
        try:
            return super().read_register(registeraddress, numberOfDecimals, functioncode, signed)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    # noinspection PyPep8Naming
    def write_register(self, registeraddress, value, numberOfDecimals=0, functioncode=6, signed=False):
        try:
            return super().write_register(registeraddress, value, numberOfDecimals, functioncode, signed)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    def start_analysis(self):
        try:
            self.write_register(50118 - 40001, 0x01)
        except ProtoError:
            pass

    def stop_analysis(self):
        try:
            self.write_register(50119 - 40001, 0x01)
        except ProtoError:
            pass

    def start_calibration(self):
        try:
            self.write_register(50120 - 40001, 0x01)
        except ProtoError:
            pass

    def start_blank(self):
        try:
            self.write_register(50121 - 40001, 0x01)
        except ProtoError:
            pass

    def start_wash(self):
        try:
            self.write_register(50122 - 40001, 0x01)
        except ProtoError:
            pass

    @repeatedly(3)
    def read_status(self):
        res = self.read_register(40191 - 40001)
        if res < 1 or res > 8:
            raise RespondError('Invalid status ' + str(res))

        return res

    @repeatedly(3)
    def read_result(self):
        a = self.read_long(40101 - 40001)
        m = bytearray(a.to_bytes(length=4, byteorder='big'))
        t = m[0]
        m[0] = m[1]
        m[1] = t
        t = m[2]
        m[2] = m[3]
        m[3] = t
        res, = struct.unpack('<f', bytes(m))
        return res


class Micromac(Device):
    timestamp = TempState('timestamp', float, 0.0)
    pump_started = TempState('pump_started', int, 0)
    analysis_started = TempState('analysis_started', int, 0)
    blank_active = TempState('blank_active', int, 0)
    wash_active = TempState('wash_active', int, 0)
    calibrate_active = TempState('calibrate_active', int, 0)
    blank_last = TempState('blank_last', float, 0)
    wash_last = TempState('wash_last', float, 0)
    calibrate_last = TempState('calibrate_last', float, 0)
    calibrate_n = TempState('calibrate_n', int, 0)
    measure_last = TempState('measure_last', float, 0)
    os = TempState('os', int, 0)

    pump_before = Configurable('pump_before', int, 5 * 60)
    pump_after = Configurable('pump_after', int, 5 * 60)
    blank_enabled = Configurable('blank_enabled', int, 0)
    wash_enabled = Configurable('wash_enabled', int, 0)
    calibrate_enabled = Configurable('calibrate_enabled', int, 0)
    calibrate_count = Configurable('calibrate_count', int, 1)
    wash_interval = Configurable('wash_interval', int, 0)
    blank_interval = Configurable('blank_interval', int, 0)
    calibrate_interval = Configurable('calibrate_interval', int, 0)

    def __init__(self, code, pump_ctl):
        super().__init__(code)
        self.pump_ctl = pump_ctl
        # self.proto = self.create_proto()

    def create_proto(self):
        proto = MicromacProto(self.port, self.address)
        proto.serial.baudrate = self.baudrate
        proto.serial.timeout = self.timeout
        return proto

    def start_analysis(self):

        proto = self.create_proto()
        if self.analysis_started == 0:
            logging.info('Micromac: starting analysis ' + self.code)
            proto.stop_analysis()
            proto.start_analysis()
            self.analysis_started = 1

    pump = {}

    def start_pump(self):
        logging.info('Micromac: starting pump ' + self.code)
        # if self.pump_started == 0:
        self.pump_ctl(1)
        self.pump_started = 1
        Micromac.pump[self.code] = 1

    def stop_pump(self):
        logging.info('Micromac: stopping pump ' + self.code)

        if self.pump_started == 1:
            self.pump_started = 0
            Micromac.pump[self.code] = 0
            for c in Micromac.pump:
                if Micromac.pump[c] == 1:
                    logging.info('Micromac: pump not stopped. Needed by ' + c)
                    return
            self.pump_ctl(0)

    def stop_analysis(self):
        if self.analysis_started == 1:
            self.analysis_started = 0

    def read_value(self):
        # logging.info('Micromac: +++read_value')
        old_status = self.status
        self.status = Device.STATUS_FAIL
        proto = self.create_proto()
        value = 0
        try:
            dev_state = proto.read_status()
        except Exception:
            self.stop_analysis()
            self.stop_pump()
            raise

        ds = datetime.fromtimestamp(self.timestamp)
        # logging.info('Micromac: status={} and dev_state={}'.format(old_status, dev_state))

        # Если идет промывка, ничего не начинаем, говорим зайти поздже
        if Laboratory.get_lab().status == Laboratory.STATUS_WASHING or not Laboratory.get_lab().cam_filled():
            # logging.info('Micromac: prom is active, waiting ' + self.code)
            # self.stop_pump()
            # self.stop_analysis()
            return 0, Device.STATUS_BUSY

        if dev_state == MicromacProto.STATE_CALIBRATION \
                or dev_state == MicromacProto.STATE_BLANK \
                or dev_state == MicromacProto.STATE_WASH \
                or dev_state == MicromacProto.STATE_FILL \
                or dev_state == MicromacProto.STATE_CLEAN \
                or self.calibrate_active == 1 \
                or self.wash_active == 1 \
                or self.blank_active == 1:
            # logging.info('Micromac: calibration is active, waiting ' + self.code)
            # self.stop_pump()
            # self.stop_analysis()
            return 0, Device.STATUS_BUSY

        # Еще не начинали мерять
        if old_status != Device.STATUS_BUSY:
            # logging.info('Micromac: old_status != Device.STATUS_BUSY')
            if dev_state == MicromacProto.STATE_STANDBY or dev_state == MicromacProto.STATE_ALARM:
                # Запуск анализа, сначала насос
                # logging.info('Micromac: dev_state == self.proto.STATE_STANDBY. Starting pump')
                if self.pump_started == 0:
                    self.timestamp = datetime.now().timestamp()
                    ds = datetime.now()
                self.start_pump()
                self.status = Device.STATUS_BUSY
            elif dev_state == MicromacProto.STATE_ANALYSIS:
                self.status = Device.STATUS_BUSY
            else:
                # По идее такого быть не должно
                logging.error('Micromac: status={} and dev_state={} '.format(old_status, dev_state) + self.code)
                self.stop_pump()
                self.stop_analysis()
                self.status = Device.STATUS_FAIL
        # В процессе измерения old_status == Device.STATUS_BUSY
        else:
            # logging.info('Micromac: old_status == Device.STATUS_BUSY')
            if dev_state == MicromacProto.STATE_STANDBY or dev_state == MicromacProto.STATE_ALARM:
                # logging.info('Micromac: dev_state == self.proto.STATE_STANDBY')
                if self.analysis_started == 0:
                    # Насос проработал 5 минут, запуск анализа
                    if (datetime.now() - ds).total_seconds() >= self.pump_before:
                        # logging.info('Micromac: datetime.now() - ds).total_seconds() >= 5 * 60')
                        self.start_analysis()
                        self.measure_last = datetime.now().timestamp()
                    self.status = Device.STATUS_BUSY
                else:
                    logging.info('Micromac: analysis complete ' + self.code)
                    value = proto.read_result()
                    self.stop_pump()
                    self.stop_analysis()
                    self.status = Device.STATUS_OK
            elif dev_state == MicromacProto.STATE_ANALYSIS:
                # logging.info('Micromac: dev_state == self.proto.STATE_ANALYSIS ' + self.code)
                self.start_analysis()
                self.status = Device.STATUS_BUSY
            else:
                logging.error('Micromac: status is not STATE_STANDBY and not STATE_ANALYSIS ' + self.code)
                self.stop_pump()
                self.stop_analysis()
                self.status = Device.STATUS_FAIL

        # Выключаем насос через 5 минут
        # logging.info('now={0} ds={1}'.format(datetime.now(), ds))
        if (datetime.now() - ds).total_seconds() >= self.pump_before + self.pump_after and self.pump_started == 1:
            # logging.info('Micromac: (datetime.now() - ds).total_seconds() >= 10 * 60 and self.pump_started == 1')
            self.stop_pump()

        # if self.pump_started == 1:
        #     self.pump_ctl(1)

        # logging.info('Micromac: ---read_value')
        return value, self.status

    def calibrate(self):
        logging.info('Micromac: Calibrate starting ' + self.code)
        proto = self.create_proto()
        proto.start_calibration()

    def blank(self):
        logging.info('Micromac: Blank starting ' + self.code)
        proto = self.create_proto()
        proto.start_blank()

    def wash(self):
        logging.info('Micromac: Wash starting ' + self.code)
        proto = self.create_proto()
        proto.start_wash()

    def read_result(self):
        logging.info('Micromac: read_result ' + self.code)
        proto = self.create_proto()
        res = proto.read_result()
        logging.info('Micromac: ... ' + str(res) + ' ' + self.code)
        return res

    @property
    def cur_status(self):
        proto = self.create_proto()
        return proto.read_status()

    def check_state(self):
        s = self.cur_status
        if self.cur_status == MicromacProto.STATE_ANALYSIS and self.os != MicromacProto.STATE_ANALYSIS:
            logging.info('Micromac: {} manual analysis. Starting pump'.format(self.code))
            self.start_pump()
            # self.analysis_started = 1
            self.timestamp = datetime.now().timestamp() - self.pump_before
        self.os = s
        # Проверим насос
        ds = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - ds).total_seconds() >= self.pump_before + self.pump_after and self.pump_started == 1:
            # logging.info('Micromac: (datetime.now() - ds).total_seconds() >= 10 * 60 and self.pump_started == 1')
            self.stop_pump()

        if self.pump_started == 1:

            Laboratory.get_lab().tags['pump_probe_state'].update()
            pump_real_state = Laboratory.get_lab().tags['pump_probe_state'].value
            if pump_real_state == 1:
                # сигнал инверсный !!!!!
                logging.info('Micromac: {} pump started but not working!'.format(self.code))
                self.start_pump()

        if self.cur_status != MicromacProto.STATE_STANDBY and self.cur_status != MicromacProto.STATE_ALARM:
            return
        if self.status == Device.STATUS_BUSY:
            return

        if self.blank_active == 1 and self.calibrate_n == 0:
            self.calibrate()
            self.calibrate_n += 1
            return

        # Повторные калибровки для хлоридов
        if self.calibrate_active == 1 and self.calibrate_count == 2 and self.calibrate_n < 1:
            cal_val = self.read_result()
            if cal_val < 41:
                self.calibrate()
                self.calibrate_n += 1
                self.calibrate_last = datetime.now().timestamp()
                return

        self.blank_active = 0
        self.wash_active = 0
        self.calibrate_active = 0
        self.calibrate_n = 0

        blank_date = datetime.fromtimestamp(self.blank_last)
        wash_date = datetime.fromtimestamp(self.wash_last)
        calibrate_date = datetime.fromtimestamp(self.calibrate_last)
        measure_date = datetime.fromtimestamp(self.measure_last)

        if self.wash_enabled == 1:
            if ((datetime.now() - wash_date).total_seconds() > self.wash_interval
                or ((datetime.now() - measure_date).total_seconds() > 60 * 60 * 24 * 3
                    and (datetime.now() - wash_date).total_seconds() > 60 * 60 * 24 * 3)):
                self.wash_active = 1
                self.wash()
                self.wash_last = datetime.now().timestamp()
                return

        if self.blank_enabled == 1:
            if (datetime.now() - blank_date).total_seconds() > self.blank_interval:
                self.blank_active = 1
                self.blank()
                self.blank_last = datetime.now().timestamp()
                return

        if self.calibrate_enabled == 1:
            if (datetime.now() - calibrate_date).total_seconds() > self.calibrate_interval:
                self.calibrate_active = 1
                self.calibrate()
                self.calibrate_last = datetime.now().timestamp()
                return

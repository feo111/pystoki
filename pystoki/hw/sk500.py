import logging
import time
from datetime import datetime

import serial
import serial.rs485
import struct

from .device import Device, LengthError, ProtoError, CRCError, RespondError, NoRespondError
from ..utils import Configurable
from ..utils import repeatedly


class ZSW(object):
    ready_to_start = False
    ready_to_operation = False
    operation_enabled = False
    fault = False
    no_off_2 = False
    no_off_3 = False
    starting_disabled = False
    warning = False
    actual_value_ok = False
    guidance_requested = False
    bit_10_active = False
    rotation_direction_right = False
    rotation_direction_left = False
    bit_13_active = False
    active_parameters_set = 1

    _zsw = 0

    fields = ('ready_to_start',
              'ready_to_operation',
              'operation_enabled',
              'fault',
              'no_off_2',
              'no_off_3',
              'starting_disabled',
              'warning',
              'actual_value_ok',
              'guidance_requested',
              'bit_10_active',
              'rotation_direction_right',
              'rotation_direction_left',
              'bit_13_active',
              )

    states = {
        'not_ready_to_start': [False, False, False, False, 'X', 'X', False, ],
        'starting_disable': [False, False, False, False, 'X', 'X', True, ],
        'ready_to_start': [True, False, False, False, True, True, False, ],
        'activated': [True, True, False, False, True, True, False, ],
        'operation_enabled': [True, True, True, False, True, True, False, ],
        'fault': [False, False, False, True, 'X', 'X', False, ],
        'error_active': [True, True, True, True, 'X', 'X', False, ],
        'emergency_stop_active': [True, True, True, False, True, False, False, ],
    }

    def __init__(self, zsw=0):
        self.from_word(zsw)

    def from_word(self, w):
        for i in range(14):
            if ((1 << i) & w) == 0:
                setattr(self, self.fields[i], False)
            else:
                setattr(self, self.fields[i], True)
        self.active_parameters_set = (w >> 14) + 1

    def to_word(self):
        res = 0
        for i, f in enumerate(self.fields):
            if getattr(self, f):
                res |= 1 << i
        res += (self.active_parameters_set - 1) << 14
        return res

    def check_state(self, st):
        state = self.states[st]
        for i, v in enumerate(state):
            if type(v) is bool:
                if getattr(self, self.fields[i]) != v:
                    return False
        return True

    @property
    def state(self):
        for k in self.states:
            if self.check_state(k):
                return k
        raise ProtoError('Wrong ZSW state', received=self.to_word())

    def __str__(self):
        r = 'STW: ('
        for k in self.fields:
            r += '{}={}; '.format(k, getattr(self, k))
        r += '{}={})'.format('active_parameters_set', self.active_parameters_set)
        r += 'Hex: 0x{:04X}'.format(self.to_word())
        r += 'State: ' + self.state
        return r


class STW(object):
    ready_for_operation = False
    off_2_canceled = False
    off_3_canceled = False
    enable_operation = False
    enable_ramp_generator = False
    enable_setpoint_on_ramp_generator = False
    enable_setpoint = False
    acknowledge = False
    bit_8_active = False
    bit_9_active = False
    pzd_valid = False
    rotation_direction_right = False
    rotation_direction_left = False
    reserved = False
    activate_parameter_set = 1
    fields = (
        'ready_for_operation',
        'off_2_canceled',
        'off_3_canceled',
        'enable_operation',
        'enable_ramp_generator',
        'enable_setpoint_on_ramp_generator',
        'enable_setpoint',
        'acknowledge',
        'bit_8_active',
        'bit_9_active',
        'pzd_valid',
        'rotation_direction_right',
        'rotation_direction_left',
        'reserved',
    )

    def __init__(self, w=0, **kwargs):
        self.from_word(w)
        for k, v in kwargs:
            if k in self.fields:
                setattr(self, k, v)
            elif k == 'activate_parameter_set':
                self.activate_parameter_set = v
            else:
                raise ValueError('Wrong keyword argument ' + k)

    def from_word(self, w):
        for i in range(14):
            if ((1 << i) & w) == 0:
                setattr(self, self.fields[i], False)
            else:
                setattr(self, self.fields[i], True)
        self.activate_parameter_set = (w >> 14) + 1

    def to_word(self):
        res = 0
        for i, f in enumerate(self.fields):
            if getattr(self, f):
                res |= 1 << i
        res += (self.activate_parameter_set - 1) << 14
        return res

    def __str__(self):
        r = 'STW: ('
        for k in self.fields:
            r += '{}={}; '.format(k, getattr(self, k))
        r += '{}={})'.format('activate_parameter_set', self.activate_parameter_set)
        r += 'Hex: 0x{:04X}'.format(self.to_word())
        return r

    def clone(self):
        return STW(self.to_word())


class SK500Proto(object):

    def __init__(self, port, baudrate, parity):
        self.port = serial.rs485.RS485(
            port,
            baudrate=baudrate,
            parity=parity,
            timeout=0.05
        )

        self.port.close()
        self.port.rs485_mode = serial.rs485.RS485Settings()
        self.last_zsw = ZSW()
        self.last_sent = 0

    @classmethod
    def crc(cls, msg):
        if len(msg) == 0:
            return b''
        r = int(msg[0])
        for b in msg[1:]:
            r ^= int(b)
        return r.to_bytes(1, 'little')

    @classmethod
    def speed_to_sw(cls, speed):
        return int(speed * 0x4000 / 100)

    # noinspection PyAugmentAssignment
    def send_msg(self, addr, pke, ind, pwe, stw, sw):
        # Формируем сообщение

        msg = struct.pack('>bbbHHHHh', 0x02, 0x0C, addr, pke, ind, pwe, stw, sw)
        msg += self.crc(msg)
        # logging.info('SK500: msg=' + str(msg))

        # Отправляем
        self.port.open()
        td = datetime.now().timestamp() - self.last_sent
        if td < 0.02:
            time.sleep(0.02 - td)
        self.port.write(msg)
        resp = self.port.read(14)
        self.last_sent = datetime.now().timestamp()
        self.port.close()
        # logging.info('SK500: resp=' + str(resp))

        # Проверяем длинну ответа
        if len(resp) == 0:
            raise NoRespondError('Device not respond', sent=msg)
        if len(resp) < 11:
            raise LengthError('Wrong respond length', sent=msg, received=resp)

        # Проверяем CRC
        resp = msg[:14 - len(resp)] + resp
        r_crc = self.crc(resp[:-1])
        if r_crc != resp[-1:]:
            raise CRCError('Error in CRC', sent=msg, received=resp)

        # Разбираем ответ на составляющие
        stx, lge, adr, pke, ind, pwe, zsw, iw, crc = struct.unpack('>bbbHHHHhb', resp)
        if stx != 0x02:
            raise RespondError('Wrong STX', sent=msg, received=resp)
        if lge != 0x0C:
            raise RespondError('Wrong LGE', sent=msg, received=resp)
        if adr != addr:
            raise RespondError('Wrong ADR', sent=msg, received=resp)

        # Разбираем слово состояния
        self.last_zsw = ZSW(zsw)
        # logging.info('Received: zsw: {}\n iw: {:04X}'.format(str(self.last_zsw), iw))
        if self.last_zsw.state == 'fault':
            raise RespondError('Device fault', sent=msg, received=resp)
        return pke, ind, pwe, zsw, iw

    def create_stw_sw(self, speed):
        # logging.info('Creating stw for last zsw: {}'.format(str(self.last_zsw)))
        stw = STW()
        stw.pzd_valid = True
        sw = 0
        try:
            state = self.last_zsw.state
        except ProtoError:
            state = 'not_ready_to_start'

        if state in ('not_ready_to_start', 'starting_disable', 'emergency_stop_active'):
            stw.ready_for_operation = False
            stw.off_2_canceled = True
            stw.off_3_canceled = True
            stw.enable_operation = True
            stw.enable_ramp_generator = True
            stw.enable_setpoint_on_ramp_generator = True
            stw.enable_setpoint = True
        elif state in ('ready_to_start', 'activated', 'operation_enabled'):
            stw.ready_for_operation = True
            stw.off_2_canceled = True
            stw.off_3_canceled = True
            stw.enable_operation = True
            stw.enable_ramp_generator = True
            stw.enable_setpoint_on_ramp_generator = True
            stw.enable_setpoint = True
            sw = self.speed_to_sw(speed)
        elif state == 'error_active':
            stw.acknowledge = True
        # logging.info('Last state="{}"'.format(state))
        # logging.info('Generated:\n{}\n{:04X}'.format(str(stw), sw))
        return stw.to_word(), sw

    def standby(self, addr):
        stw, sw = self.create_stw_sw(0)
        return self.send_msg(addr, 0, 0, 0, stw, sw)
        # return self.send_msg(addr, 0, 0, 0, 0x47e, 0)

    def set_speed(self, addr, speed):
        # return self.send_msg(addr, 0, 0, 0, 0x47f, int(speed * 0x4000 / 100))
        stw, sw = self.create_stw_sw(speed)
        _, _, _, _, iw = self.send_msg(addr, 0, 0, 0, stw, sw)
        while not (self.last_zsw.operation_enabled and self.last_zsw.actual_value_ok):
            stw, sw = self.create_stw_sw(speed)
            _, _, _, _, iw = self.send_msg(addr, 0, 0, 0, stw, sw)
        return iw * 100 / 0x4000

    def read_parameter(self, addr, param):
        ak = 1
        pke = (ak << 12) & (param & 0x7ff)
        ind = 0
        pwe = 0
        self.send_msg(addr, pke, ind, pwe, 0, 0)


class SK500(Device):

    speed = Configurable('speed', float, default=50)

    def __init__(self, code):
        super().__init__(code)
        self.proto = SK500Proto(self.port, self.baudrate, self.parity)

    @repeatedly(3, ProtoError)
    def standby(self):
        self.status = self.STATUS_FAIL
        self.proto.standby(self.address)
        self.status = self.STATUS_OK

    @repeatedly(3)
    def set_speed(self, speed):
        self.speed = speed
        self.status = self.STATUS_FAIL
        self.proto.set_speed(self.address, self.speed)
        self.status = self.STATUS_OK

    @property
    def cur_speed(self):
        self.status = self.STATUS_FAIL
        speed = self.proto.set_speed(self.address, self.speed)
        self.status = self.STATUS_OK
        return speed

    @repeatedly(3)
    def run(self):
        self.set_speed(self.speed)


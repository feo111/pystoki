import logging
import sys

import minimalmodbus
import serial.rs485

from .device import Device, NoRespondError, RespondError, ProtoError
from ..utils import Configurable


class VegaProto(minimalmodbus.Instrument):

    def __init__(self, port, address):
        super().__init__(port, address)
        self.close_port_after_each_call = True
        self.serial.close()
        self.serial = serial.rs485.RS485(
            port,
            baudrate=self.serial.baudrate,
            timeout=self.serial.timeout,
            )
        self.serial.close()
        self.serial.rs485_mode = serial.rs485.RS485Settings()

    # noinspection PyPep8Naming
    def read_float(self, registeraddress, functioncode=3, numberOfRegisters=2):
        try:
            return super().read_float(registeraddress, functioncode, numberOfRegisters)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    # noinspection PyPep8Naming
    def read_register(self, registeraddress, numberOfDecimals=0, functioncode=3, signed=False):
        try:
            return super().read_register(registeraddress, numberOfDecimals, functioncode, signed)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    def set_speed(self, speed):
        cmd2000 = 0
        if speed == 0:
            cmd2000 = 1
        if speed > 0:
            cmd2000 |= 1 << 4
            cmd2000 |= 2
        if speed < 0:
            cmd2000 |= 2
            cmd2000 |= 1 << 5
            speed = -speed
            
        cmd2001 = round(speed * 100)
        self.write_register(0x2000, cmd2000, functioncode=6)
        self.write_register(0x2001, cmd2001, functioncode=6)

    def get_cur_speed(self):
        res = self.read_register(0x2103)
        return res / 100

    def get_target_speed(self):
        res = self.read_register(0x2102)
        return res / 100


class Vega(Device):

    speed = Configurable('speed', float, default=50)

    def __init__(self, code):
        super().__init__(code)

    def create_proto(self):
        proto = VegaProto(self.port, self.address)
        proto.serial.baudrate = self.baudrate
        proto.serial.timeout = self.timeout
        return proto

    def set_speed(self, speed):
        logging.info('VEGA: setting speed {}'.format(speed))
        self.speed = speed
        self.status = self.STATUS_FAIL
        proto = self.create_proto()
        proto.set_speed(speed)
        self.status = self.STATUS_OK

    @property
    def cur_speed(self):
        self.status = self.STATUS_FAIL
        proto = self.create_proto()
        res = proto.get_cur_speed()
        self.status = self.STATUS_OK
        return res

    def run(self):
        self.set_speed(self.speed)

    def check_state(self):
        if self.cur_speed != self.speed:
            self.run()

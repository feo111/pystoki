import serial
import serial.rs485


class Port(object):
    def __init__(self, dev_name, baudrate=9600,
                 bytesize=serial.EIGHTBITS,
                 parity=serial.PARITY_NONE,
                 stopbits=serial.STOPBITS_ONE,
                 timeout=None, xonxoff=False,
                 rtscts=False,
                 dsrdtr=False,):
        self.dev_name = dev_name
        self.config = {
            'baudrate': baudrate,
            'bytesize': bytesize,
            'parity': parity,
            'stopbits': stopbits,
            'timeout': timeout,
            'xonxoff': xonxoff,
            'rtscts': rtscts,
            'dsrdtr': dsrdtr,
        }
        self.port = None

    def open(self):
        if self.port is None:
            self.port = serial.Serial(self.dev_name)
        else:
            self.port.open()
        self.set_config()

    def set_config(self):
        self.port.baudrate = self.config['baudrate']
        self.port.bytesize = self.config['bytesize']
        self.port.parity = self.config['parity']
        self.port.stopbits = self.config['stopbits']
        self.port.timeout = self.config['timeout']
        self.port.xonxoff = self.config['xonxoff']
        self.port.rtscts = self.config['rtscts']
        self.port.dsrdtr = self.config['dsrdtr']

    def close(self):
        self.port.close()

    def is_open(self):
        return self.port.isOpen()

    def write(self, data):
        return self.port.write(data)

    def read(self, size=1):
        return self.port.read(size)

    def __enter__(self):
        self.open()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False

    def __repr__(self):
        return '<Port>:' + repr(self.port)


class PortRS485(Port):

    def get_serial(self):
        if self.port is None:
            self.port = serial.rs485.RS485()
            self.set_config()
            self.port.rs485_mode = serial.rs485.RS485Settings()
            self.port.port = self.dev_name
        return self.port

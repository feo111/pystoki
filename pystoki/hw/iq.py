import sys

import minimalmodbus
import serial.rs485

from .device import Device, NoRespondError, RespondError, ProtoError
from ..utils import repeatedly


class IQProto(minimalmodbus.Instrument):

    def __init__(self, port, address):
        super().__init__(port, address)
        self.close_port_after_each_call = True
        self.serial.close()
        self.serial = serial.rs485.RS485(
            port,
            baudrate=self.serial.baudrate,
            timeout=self.serial.timeout,

            )
        self.serial.close()
        self.serial.rs485_mode = serial.rs485.RS485Settings()

    # noinspection PyPep8Naming
    def read_float(self, registeraddress, functioncode=3, numberOfRegisters=2):
        try:
            return super().read_float(registeraddress, functioncode, numberOfRegisters)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)


class IQ(Device):

    ADDRESS_F = 100
    ADDRESS_FTOT = 106
    ADDRESS_LEVEL = 102
    ADDRESS_TEMP = 114

    def __init__(self, code):
        super().__init__(code)
        # self.proto = self.create_proto()

    def create_proto(self):
        proto = IQProto(self.port, self.address)
        proto.serial.baudrate = self.baudrate
        proto.serial.timeout = self.timeout
        return proto

    @repeatedly(3, ProtoError)
    def read_float(self, address):
        self.status = Device.STATUS_FAIL
        proto = self.create_proto()
        res = proto.read_float(address)
        self.status = Device.STATUS_OK
        return res, self.status

    def read_flow(self):
        res, st = self.read_float(self.ADDRESS_F)
        return res * 3600, st

    def read_total(self):
        return self.read_float(self.ADDRESS_FTOT)

    def read_level(self):
        return self.read_float(self.ADDRESS_LEVEL)

    def read_temp(self):
        return self.read_float(self.ADDRESS_TEMP)

import sys

import minimalmodbus
import serial.rs485
import struct

from .device import Device, NoRespondError, RespondError, ProtoError
from ..utils import repeatedly


# TODO Одновремменное измерение железа и хлоридов
class SiboldProto(minimalmodbus.Instrument):

    def read_bit(self, registeraddress, functioncode=1):
        try:
            return super().read_bit(registeraddress, functioncode)
        except IOError as ex:
            raise NoRespondError('No respond', inner=ex)
        except ValueError as ex:
            raise RespondError('Bad response', inner=ex)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)

    # noinspection PyPep8Naming
    def read_float(self, registeraddress, functioncode=3, numberOfRegisters=2):
        try:
            hi, lo = super().read_registers(registeraddress, 2)
            b = struct.pack('>HH', lo, hi)
            return struct.unpack('>f', b)[0]
        except IOError as ex:
            raise NoRespondError('No respond', inner=ex)
        except ValueError as ex:
            raise RespondError('Error in modbus protocol', inner=ex)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)


class Sibold(Device):

    FE_START = 2453 - 1
    FE_READY = 13 - 1
    FE_RESULT = 4892 - 1

    CL_START = 2472 - 1
    CL_READY = 14 - 1
    CL_RESULT = 4895 - 1

    def __init__(self, code):
        super().__init__(code)

    def create_proto(self):
        proto = SiboldProto(self.port, self.address)
        proto.close_port_after_each_call = True
        proto.serial.close()
        proto.serial = serial.rs485.RS485(self.port,
                                          baudrate=self.baudrate,
                                          timeout=self.timeout,
                                          )
        proto.serial.close()
        proto.serial.rs485_mode = serial.rs485.RS485Settings()
        return proto

    @repeatedly(5, ProtoError)
    def read_cl(self):
        status = self.status
        self.status = Device.STATUS_FAIL
        value = 0
        if status != Device.STATUS_BUSY:
            proto = self.create_proto()
            proto.write_bit(self.CL_START, 1)
            self.status = Device.STATUS_BUSY
        else:
            proto = self.create_proto()
            busy = proto.read_bit(self.CL_READY) == 1
            if not busy:
                value = proto.read_float(self.CL_RESULT)
                self.status = Device.STATUS_OK
        return value, self.status

    @repeatedly(5, ProtoError)
    def read_fe(self):
        status = self.status
        self.status = Device.STATUS_FAIL
        value = 0
        if status != Device.STATUS_BUSY:
            proto = self.create_proto()
            proto.write_bit(self.FE_START, 1)
            self.status = Device.STATUS_BUSY
        else:
            proto = self.create_proto()
            busy = proto.read_bit(self.FE_READY) == 1
            if not busy:
                value = proto.read_float(self.FE_RESULT)
                self.status = Device.STATUS_OK
        return value, self.status

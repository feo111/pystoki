import sys

import minimalmodbus
import serial.rs485

from .device import Device, NoRespondError, RespondError, ProtoError
from ..utils import repeatedly, Configurable


class SC1000Proto(minimalmodbus.Instrument):

    def __init__(self, port, address):
        super().__init__(port, address)
        self.close_port_after_each_call = True
        self.serial.close()
        self.serial = serial.rs485.RS485(
            port,
            baudrate=self.serial.baudrate,
            timeout=self.serial.timeout,
            )
        self.serial.close()
        self.serial.rs485_mode = serial.rs485.RS485Settings()

    # noinspection PyPep8Naming
    def read_float(self, registeraddress, functioncode=3, numberOfRegisters=2):
        try:
            return super().read_float(registeraddress, functioncode, numberOfRegisters)
        except IOError as err:
            raise NoRespondError('No respond', inner=err)
        except ValueError as err:
            raise RespondError('Respond error', inner=err)
        except:
            et, ev, tb = sys.exc_info()
            raise ProtoError('Unknown error of type {}'.format(et), inner=ev)


class SC1000(Device):

    ADDRESS_PH = Configurable('address_ph', int, default=4)
    ADDRESS_T = Configurable('address_t', int, default=6)
    ADDRESS_VV = Configurable('address_vv', int, default=12)
    ADDRESS_OIL = Configurable('address_oil', int, default=18)

    def __init__(self, code):
        super().__init__(code)

    def create_proto(self):
        proto = SC1000Proto(self.port, self.address)
        proto.serial.baudrate = self.baudrate
        proto.serial.timeout = self.timeout
        return proto

    @repeatedly(3, ProtoError)
    def read_float(self, address):
        proto = self.create_proto()
        self.status = Device.STATUS_FAIL
        res = proto.read_float(address)
        self.status = Device.STATUS_OK
        return res, self.status

    def read_ph(self):
        return self.read_float(self.ADDRESS_PH)

    def read_vv(self):
        return self.read_float(self.ADDRESS_VV)

    def read_t(self):
        return self.read_float(self.ADDRESS_T)

    def read_oil(self):
        return self.read_float(self.ADDRESS_OIL)

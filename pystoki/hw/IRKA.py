import logging

import struct

from .device import Device, CRCError, LengthError, RespondError, ProtoError
from .port import Port
from ..utils import repeatedly


class IRKAProto(object):
    HEADER_READ = 0x25
    HEADER_WRITE = 0x2b
    HEADER_OK = 0x21
    HEADER_ERROR = 0x3f

    CMD_READ_TOTAL = 0x56
    CMD_READ_FLOW = 0x55

    def __init__(self, port):
        self.port = port

    @classmethod
    def crc(cls, msg):
        res = 0
        for b in msg:
            res += int(b)
            res %= 256
        return res.to_bytes(1, 'little')

    def pack_message(self, header, address, cmd, data):
        msg = struct.pack('bbbb', header, address, cmd, len(data))
        msg += data
        msg += self.crc(msg)
        return msg

    def unpack_message(self, msg):
        crc = msg[-1:]
        m = msg[:-1]
        if crc != self.crc(m):
            raise CRCError('Bad CRC', received=msg)
        header = int(m[0])
        address = int(m[1])
        cmd = int(m[2])
        length = int(m[3])
        data = m[4:]
        if len(data) != length:
            raise LengthError('Bad length', received=msg)
        return header, address, cmd, data

    def send_message(self, msg):
        logging.debug('IRKA: send msg={}'.format(msg))
        rst = b''
        h = 0
        with self.port:
            self.port.write(msg)
            hdr = self.port.read(4)
            if len(hdr) < 4:
                raise RespondError('Bad header received', sent=msg, received=hdr)
            h, _, _, l = struct.unpack('bbbb', hdr)
            rst = self.port.read(l + 1)
        resp = hdr + rst
        logging.debug('IRKA: recv msg={}'.format(resp))
        if h == self.HEADER_ERROR:
            raise RespondError('IRKA return error', sent=msg, received=resp)
        return resp

    def read_total(self, address):
        msg = self.pack_message(self.HEADER_READ,
                                address,
                                self.CMD_READ_TOTAL,
                                b'')
        resp = self.send_message(msg)
        h, a, c, d = self.unpack_message(resp)
        if a != address\
                or c != self.CMD_READ_TOTAL:
            raise RespondError('Bad command received', sent=msg, received=resp)
        (total,) = struct.unpack('I', d)
        if total < 0:
            raise RespondError('Bad total value', sent=msg, received=resp)
        return total

    def read_flow(self, address):
        msg = self.pack_message(self.HEADER_READ,
                                address,
                                self.CMD_READ_FLOW,
                                b'')
        resp = self.send_message(msg)
        h, a, c, d = self.unpack_message(resp)
        if a != address\
                or c != self.CMD_READ_FLOW:
            raise RespondError('Bad command received', sent=msg, received=resp)
        (flow, ) = struct.unpack('f', d)
        if flow < 0 or flow > 15000:
            raise RespondError('Bad flow value', sent=msg, received=resp)
        return flow


class IRKA(Device):

    def __init__(self, code):
        super().__init__(code)
        self.proto = IRKAProto(Port(self.port,
                                    baudrate=self.baudrate,
                                    timeout=self.timeout,
                                    ))

    @repeatedly(3, ProtoError)
    def read_flow(self):
        self.status = Device.STATUS_FAIL
        res = self.proto.read_flow(self.address)
        self.status = Device.STATUS_OK
        return res, self.status

    @repeatedly(3, ProtoError)
    def read_total(self):
        self.status = Device.STATUS_FAIL
        res = self.proto.read_total(self.address)
        self.status = Device.STATUS_OK
        if res < 0 or res >= 2**32:
            self.status = Device.STATUS_FAIL
        res = int(res / 100) * 100
        return res, self.status

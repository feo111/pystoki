from ..utils import Configurable, TempState
from datetime import datetime
import logging


class ProtoError(Exception):
    def __init__(self, text='', sent=None, received=None, inner=None):
        self.text = text
        self.sent = sent
        self.received = received
        self.inner = inner

    @classmethod
    def to_hex(cls, bytes_):
        if type(bytes_) == str:
            try:
                bytes_ = bytes_.encode('ascii')
            except UnicodeEncodeError:
                bytes_ = bytes_.encode('utf-8', 'replace')
        return ' '.join(['{:02X}'.format(b) for b in bytes_])

    def __str__(self):
        res = '{}: {}'.format(
            type(self),
            self.text,
        )
        if self.sent:
            res += '\n\tSent: {}({})'.format(self.to_hex(self.sent), str(self.sent))
        if self.received:
            res += '\n\tReceived: {}({})'.format(self.to_hex(self.received), str(self.received))
        if self.inner:
            res += '\n\tInnerException: ' + repr(self.inner)
        return res


class CRCError(ProtoError):
    pass


class LengthError(ProtoError):
    pass


class RespondError(ProtoError):
    pass


class NoRespondError(ProtoError):
    pass


class Device:
    port = Configurable('port', default='/dev/ttyS0')
    baudrate = Configurable('baudrate', int, 19200)
    parity = Configurable('parity', default='N')
    timeout = Configurable('timeout', float, 1)
    address = Configurable('address', int, 1)
    enabled = Configurable('enabled', int, 1)


    STATUS_OK = 0
    STATUS_INIT = 1
    STATUS_NOT_RESPOND = 2
    STATUS_FAIL = 3
    STATUS_BUSY = 4

    status = TempState('status', int, STATUS_OK)

    def __init__(self, code):
        self.code = code
        self.status = Device.STATUS_OK

    def check_state(self):
        pass


class TwoStateDevice(Device):
    current_state = TempState('current_state', int, 0)

    def __init__(self, code, io_module, chanel):
        super().__init__(code)
        self.io_module = io_module
        self.chanel = chanel

    def set_state(self, state):
        self.current_state = state
        self.io_module.set_output(self.chanel, state)

    def on(self):
        self.set_state(1)

    def off(self):
        self.set_state(0)

    def check_state(self):
        if self.enabled == 0:
            return
        if self.io_module.get_output(self.chanel) != self.current_state:
            self.set_state(self.current_state)

    def __call__(self, state):
        self.set_state(state)


class ZelioDevice(TwoStateDevice):
    last_change = TempState('last_change', float, 0)
    interval = Configurable('interval', int, 20)

    def check_state(self):
        super().check_state()

        last_change_time = datetime.fromtimestamp(self.last_change)
        if (datetime.now() - last_change_time).total_seconds() > self.interval:
            logging.info('zelio: Changing state')
            if self.current_state == 1:
                self.set_state(0)
            else:
                self.set_state(1)
            self.last_change = datetime.now().timestamp()

import logging
from datetime import datetime

import serial.rs485

from .device import Device, RespondError, ProtoError, CRCError, LengthError
from ..utils import repeatedly, Configurable


class I7000Proto(object):
    def __init__(self, port, address, baudrate, timeout, use_crc):
        self.port = serial.rs485.RS485(
            port,
            baudrate=baudrate,
            timeout=timeout,
        )
        self.port.close()
        self.port.rs485_mode = serial.rs485.RS485Settings()
        # self.port.open()
        self.address = address
        self.use_crc = use_crc
        self.last_msg = b''
        self.last_resp = b''

    def raise_error(self, msg='', error_type=ProtoError):
        if not issubclass(error_type, ProtoError):
            raise ValueError('I7000: invalid error_type: '.format(error_type))
        raise error_type('I7000: ' + msg, sent=self.last_msg, received=self.last_resp)

    def _read_response(self):
        end = False
        resp = b''
        n = 0
        while not end:
            ch = self.port.read()
            if ch == b'!' or ch == b'>' or ch == b'!' or ch == b'\r' or b'0' <= ch <= b'9' or b'A' <= ch <= b'F':
                resp += ch
                n += len(ch)
            if ch == b'' or ch == b'\r':
                end = True
        self.last_resp = resp
        if resp[-1:] == b'\r':
            resp = resp[:-1]
        if self.use_crc == 1:
            if self.crc(resp[:-2]) != resp[-2:]:
                self.raise_error('Bad CRC', CRCError)
            resp = resp[:-2]
        resp_str = ''
        try:
            resp_str = resp.decode('ascii')
        except UnicodeDecodeError:
            self.raise_error('Bad respond', RespondError)
        return resp_str

    @staticmethod
    def crc(msg):
        s = 0
        for b in msg:
            s += b
            s &= 0xFF
        return '{:02X}'.format(s).encode('ascii')

    def format_msg(self, msg):
        if isinstance(msg, str):
            msg = msg.encode('ascii')
        if self.use_crc == 0:
            return msg + b'\r'
        else:
            return msg + self.crc(msg) + b'\r'

    def write_command(self, msg):
        msg = self.format_msg(msg)

        self.last_msg = msg
        self.last_resp = b''
        with self.port:
            if not self.port.isOpen():
                self.port.open()
            logging.debug('I7000: msg={}'.format(msg))
            self.port.write(msg)
            resp = self._read_response()
            logging.debug('I7000: resp={}'.format(resp))
            self.port.close()
        return resp

    def read_inout(self):
        msg = '${:02X}6'.format(self.address)
        resp = self.write_command(msg)

        if len(resp) == 0:
            self.raise_error('Bad respond length {}'.format(len(resp)), LengthError)

        if resp[0] == '?':
            self.raise_error('Invalid command', RespondError)
        if resp[0] == '!':
            resp = resp[1:]
        if len(resp) != 6:
            self.raise_error('Bad respond length {}'.format(len(resp)), LengthError)
        if resp[-2:] != '00':
            self.raise_error('Bad respond', RespondError)

        o = i = 0
        try:
            o, i = int(resp[0:2], base=16), int(resp[2:4], base=16)
        except ValueError:
            self.raise_error('Bad respond', RespondError)
        return i, o

    def set_output(self, n, val):
        if n < 8:
            msg = '#{:02X}A{:1d}{:02d}'.format(self.address, n, val)
        else:
            msg = '#{:02X}B{:1d}{:02d}'.format(self.address, n - 8, val)
        resp = self.write_command(msg)
        if resp == '?':
            self.raise_error('Invalid command')
        if resp == '!':
            self.raise_error('Ignored command')
        if resp != '>':
            self.raise_error('Invalid respond', RespondError)

    def set_outputs(self, outputs):
        """
        :param outputs: array of low eight outputs
        :return: Nothing
        """
        # logging.info('I7000Proto: set_outputs enter outputs={}'.format(str(outputs)))
        if len(outputs) == 0:
            raise ValueError('Wrong outputs number')
        byte = 0
        for i in range(len(outputs) if len(outputs) <= 8 else 8):
            byte |= outputs[i] << i
        msg = '#{:02X}0A{:02X}'.format(self.address, byte)
        resp = self.write_command(msg)
        if resp == '?':
            self.raise_error('Invalid command')
        if resp == '!':
            self.raise_error('Ignored command')
        if resp != '>':
            self.raise_error('Invalid respond', RespondError)

        if len(outputs) > 8:
            byte = 0
            for i in range(len(outputs) - 8):
                byte |= outputs[i + 8] << i
            msg = '#{:02X}0B{:02X}'.format(self.address, byte)
            resp = self.write_command(msg)
            if resp == '?':
                self.raise_error('Invalid command')
            if resp == '!':
                self.raise_error('Ignored command')
            if resp != '>':
                self.raise_error('Invalid respond', RespondError)


class I7000(Device):
    _inputs = 0
    _outputs = 0
    _last_read = datetime(1900, 1, 1)
    _state = Device.STATUS_OK

    OUTPUTS_COUNT = 4

    use_crc = Configurable('use_crc', int, 0)

    def __init__(self, code):
        super().__init__(code)
        self.outputs_states = [0] * self.OUTPUTS_COUNT

    def _get_proto(self):
        proto = I7000Proto(self.port, self.address, self.baudrate, self.timeout, self.use_crc)
        return proto

    @repeatedly(3, ProtoError)
    def _read_inout(self):
        proto = self._get_proto()
        self._state = Device.STATUS_FAIL
        old_outs, old_inputs = self._outputs, self._inputs
        self._inputs, self._outputs = proto.read_inout()
        if old_outs != self._outputs or old_inputs != self._inputs:
            logging.info('I7000: {} outputs=0x{:02X} inputs=0x{:02X}'.format(self.code, self._outputs, self._inputs))
        self._state = Device.STATUS_OK
        self._last_read = datetime.now()

    @property
    def inputs(self):
        # if (datetime.now() - self._last_read).total_seconds() > 5:
        self._read_inout()
        return self._inputs

    @property
    def outputs(self):
        # if (datetime.now() - self._last_read).total_seconds() > 5:
        self._read_inout()
        return self._outputs

    def get_input(self, n):
        return 0 if self.inputs & (1 << n) != 0 else 1, self._state

    def get_output(self, n):
        self._read_inout()
        return 1 if self.outputs & (1 << n) != 0 else 0

    def set_output(self, n, val):
        # logging.info('I7000: enter set_outputs n={} val={} outputs_states={}'.format(n,val,str(self.outputs_states)))
        self.outputs_states[n] = val
        self.set_outputs()
        # logging.info('I7000: exit set_outputs n={} val={} outputs_states={}'.format(n, val, str(self.outputs_states)))

    @repeatedly(3, ProtoError)
    def set_outputs(self):
        proto = self._get_proto()
        proto.set_outputs(self.outputs_states)

    def check_state(self):
        self.set_outputs()
        self._read_inout()


class I7061(I7000):
    OUTPUTS_COUNT = 12

    def _read_inout(self):
        proto = self._get_proto()
        self._state = Device.STATUS_FAIL
        old_outs = self._outputs
        o_l, o_h = proto.read_inout()
        self._outputs = (o_h << 8) | o_l
        if old_outs != self._outputs:
            logging.info('I7000: {} outputs=0x{:04X}'.format(self.code, self._outputs))
        self._state = Device.STATUS_OK
        self._last_read = datetime.now()

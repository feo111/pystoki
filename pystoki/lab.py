import logging
import time
from datetime import datetime, timedelta

from .messages import Dispatcher
from .utils import Configurable, TempState


# class LabStatus(Enum):
#     stopped = 0
#     running = 1
#     washing = 2


# noinspection PyBroadException
class Laboratory(object):
    STATUS_STOPPED = 0
    STATUS_RUNNING = 1
    STATUS_WASHING = 2

    status = TempState('status', int, STATUS_STOPPED)
    tags = {}

    circle_time = Configurable('circle_time', float, 1.0)
    server_url = Configurable('server_url')
    proxy = Configurable('proxy')
    messages_in_packet = Configurable('messages_in_packet', int, 50)
    sending_messages = Configurable('sending_messages', int, 1)

    __cur_lab = None

    def __init__(self, code):
        self.code = code
        self.tags = {}
        self.tasks = []
        self.devices = {}
        self.dispatcher = Dispatcher(self.code,
                                     self.server_url,
                                     self.proxy,
                                     self.messages_in_packet,
                                     )
        self.archive = self.dispatcher.archive
        Laboratory.__cur_lab = self

    @classmethod
    def get_lab(cls):
        return Laboratory.__cur_lab

    def run(self):
        self.status = Laboratory.STATUS_RUNNING
        if self.sending_messages == 1:
            self.dispatcher.start()
        while self.status != Laboratory.STATUS_STOPPED:
            # ts = time.perf_counter()
            try:
                self.circle()
            except Exception:
                logging.exception('Error')

    def circle(self):
        for code, device in self.devices.items():
            try:
                if device.enabled == 1:
                    device.check_state()
            except Exception:
                logging.exception('Device check error code={}'.format(code))

        for code, task in self.tasks:
            try:
                task.start()
            except Exception:
                logging.exception('Task error ' + code)

        for code, tag in self.tags.items():
            if next_update(tag.date, tag.interval) <= datetime.now() or tag.quality == tag.QUALITY_MEASURING:
                try:
                    tag.update()
                except Exception:
                    logging.exception('Error on tag {} update'.format(code))


def next_update(last_date, interval):
    next_update_max = last_date.replace(microsecond=0) + timedelta(seconds=interval)
    result = last_date.replace(microsecond=0)
    if interval % 60 == 0:
        result = result.replace(second=0)
        if 3600 % interval == 0 or interval % 3600 == 0:
            result = result.replace(minute=0)
    while result + timedelta(seconds=interval) <= next_update_max:
        result += timedelta(seconds=interval)
    return result

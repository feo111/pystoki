from pystoki.utils import Configurable, TempState
import logging
from datetime import datetime


class Task(object):
    enabled = Configurable('enabled', int, default=1)

    def __init__(self, code, lab):
        self.code = code
        self.lab = lab

    def start(self):
        if self.enabled == 1:
            # logging.info('Task: run ' + self.code)
            self.run()

    def run(self):
        # logging.info('Task: run ' + self.code)
        pass


class TimedStateMachine(Task):
    states = []
    cur_state = TempState('cur_state', int, 0)
    timestamp = TempState('timestamp', float, default=0.0)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    def run(self):
        # logging.info('Task: run ' + self.code)
        state = self.states[self.cur_state]
        dt = datetime.fromtimestamp(self.timestamp)
        nxt = state.next(dt, self.lab)
        if nxt != state:
            self.cur_state = self.states.index(nxt)
            self.timestamp = datetime.now().timestamp()


class TimedState(object):
    def __init__(self, code, duration, next_state=None, on_enter_f=None, on_leave_f=None, checks=list()):
        self.code = code
        self.duration = duration
        self.next_state = next_state
        self.on_enter_f = on_enter_f
        self.on_leave_f = on_leave_f
        self.checks = checks

    def next(self, date_start, lab):
        if self.check_next(date_start, lab):
            logging.info('{}: leave'.format(self.code))
            self.on_leave(lab)

            logging.info('{}: enter'.format(self.next_state.code))
            self.next_state.on_enter(lab)

            return self.next_state
        else:
            self.on_active(lab)
            return self

    def check_next(self, date_start, lab):
        if (datetime.now() - date_start).total_seconds() < self.duration:
            return False
        for c in self.checks:
            if not c(lab):
                return False
        return True

    def on_enter(self, lab):
        if self.on_enter_f is not None:
            self.on_enter_f(lab)

    def on_leave(self, lab):
        if self.on_leave_f is not None:
            self.on_leave_f(lab)

    def on_active(self, lab):
        pass

from datetime import datetime
from .task import Task
from ..utils import Configurable, TempState


class Calibrate(Task):
    interval = Configurable('interval', int, 12 * 60 * 60)
    timestamp = TempState('timestamp', float, 0.0)

    def __init__(self, code, lab, device):
        super().__init__(code, lab)
        self.device = device

    def run(self):
        # logging.info('Task: run ' + self.code)
        self.device.check_states()

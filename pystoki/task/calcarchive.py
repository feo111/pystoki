import logging
from datetime import datetime, timedelta

from pystoki.lab import Laboratory
from pystoki.tags import Tag
from pystoki.utils import Configurable, TempState
from .task import Task


class CalculateHoursValues(Task):

    write_interval = Configurable('write_interval', int, default=60)

    prev_hour_stamp = TempState('prev_hour_stamp', datetime)
    prev_ftot = TempState('prev_ftot', float, default=0)
    last_update_stamp = TempState('last_update', datetime)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    @classmethod
    def send_message(cls, code, date, value):
        logging.info('Hourly: {}, val={}, date={}'.format(code, value, date))
        msg = {
            'code': code,
            'type': 'archive_hourly',
            'date': date,
            'value': value,
            'quality': 0,
        }
        Laboratory.get_lab().dispatcher.send_message(msg)

    def tag_avg(self, tag_name, hour):
        data = self.lab.archive.read(hour, hour.replace(minute=59, second=59), 'current', tag_name)
        avg_data = self.lab.tags[tag_name].last_value
        data = [float(d.value) for d in data if int(d.quality) < 2]
        if len(data) > 0:
            avg_data = sum(data) / len(data)
        res = round(avg_data, self.lab.tags[tag_name].precision)
        return res

    def run(self):
        now = datetime.now().replace(microsecond=0)
        ht = now.replace(minute=0,
                         second=0,
                         microsecond=0,
                         )
        # pt = ht - timedelta(hours=1)

        tags = self.lab.tags

        if self.prev_hour_stamp == datetime.min:
            self.prev_hour_stamp = ht
            self.prev_ftot = tags['ftot'].last_value
        prev_hour = self.prev_hour_stamp

        if self.last_update_stamp == datetime.min:
            self.last_update_stamp = now
        # last_update = datetime.fromtimestamp(self.last_update_stamp)

        if (now - prev_hour).total_seconds() >= 60 * 60:
            it = prev_hour + timedelta(hours=1)
            while it < ht:
                pt = it
                # Ждем измерения в новом часе
                # if tags['ftot'].date < pt:
                #    logging.info('Hourly: ')

                # Обновим суммарный расход
                logging.info('Hourly: ftot updating')
                if not tags['ftot'].update():
                    return
                if tags['ftot'].quality > Tag.QUALITY_WARNING:
                    logging.info('Hourly: ftot quality is bad, dont calc archive')
                    return
                logging.info(
                    ('Hourly: pt="{:%d.%m.%Y %H:%M:%S}" ht="{:%d.%m.%Y %H:%M:%S}" '
                     'prev_hour="{:%d.%m.%Y %H:%M:%S}"').format(pt, ht, prev_hour))

                # Считаем расход за прошлый час
                q_val = tags['ftot'].last_value - self.prev_ftot
                if q_val < 0 or q_val > 15000:
                    logging.info('Hourly: q_val={}, setting to 0'.format(q_val))
                    q_val = 0
                # if q_val < 0:
                #    q_val = 0

                # Считаем средние значения за прошлый час
                aph = self.tag_avg('ph', pt)
                af = self.tag_avg('f', pt)
                avv = self.tag_avg('vv', pt)
                a_oil = self.tag_avg('oil', pt)
                afe = self.tag_avg('fe', pt)
                acl = self.tag_avg('cl', pt)
                at = self.tag_avg('t', pt)

                # Массы
                if q_val > 0:
                    mvv = avv * q_val
                    moil = a_oil * q_val
                    mfe = afe * q_val
                    mcl = acl * q_val
                else:
                    mvv = moil = mfe = mcl = 0

                # Отправляем сообщения
                self.send_message('ftot', pt, q_val)
                self.send_message('mvv', pt, mvv)
                self.send_message('mcl', pt, mcl)
                self.send_message('mfe', pt, mfe)
                self.send_message('moil', pt, moil)

                self.send_message('ph', pt, aph)
                self.send_message('f', pt, af)
                self.send_message('vv', pt, avv)
                self.send_message('oil', pt, a_oil)
                self.send_message('fe', pt, afe)
                self.send_message('cl', pt, acl)
                self.send_message('t', pt, at)

                # запоминаем показания расходомера и время
                self.prev_ftot = tags['ftot'].last_value
                self.prev_hour_stamp = pt

                it += timedelta(hours=1)


class CalculateDailyValues(Task):

    prev_day_stamp = TempState('prev_day_stamp', float, default=0)
    prev_ftot = TempState('prev_ftot', float, default=0)
    # avg_ph = TempState('avg_ph', float, default=0)
    # avg_t = TempState('avg_t', float, default=0)
    # mvv = TempState('m_vv', float, default=0)
    # moil = TempState('m_oil', float, default=0)
    # mfe = TempState('m_fe', float, default=0)
    # mcl = TempState('m_cl', float, default=0)
    # avg_f = TempState('avg_f', float, default=0)
    # count_ph = TempState('count_ph', int, default=0)
    # count_f = TempState('count_f', int, default=0)
    # count_t = TempState('count_t', int, default=0)
    last_update_stamp = TempState('last_update', float, default=0)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    @staticmethod
    def send_message(code, date, value):
        logging.info('Daily: {}, val={}, date={}'.format(code, value, date))
        msg = {
            'code': code,
            'type': 'archive_dayly',
            'date': date,
            'value': value,
            'quality': 0,
        }
        Laboratory.get_lab().dispatcher.send_message(msg)

    def tag_avg(self, tag_name, day):
        data = self.lab.archive.read(day, day.replace(hour=23, minute=59, second=59), 'archive_hourly', tag_name)
        avg_data = self.lab.tags[tag_name].last_value
        data = [float(d.value) for d in data if int(d.quality) < 2]
        if len(data) > 0:
            avg_data = sum(data) / len(data)
        res = round(avg_data, self.lab.tags[tag_name].precision)
        return res

    def tag_sum(self, tag_name, day):
        data = self.lab.archive.read(day, day.replace(hour=23, minute=59, second=59), 'archive_hourly', tag_name)
        data = [float(d.value) for d in data if int(d.quality) < 2]
        tp_name = tag_name
        if tp_name[0] == 'm':
            tp_name = tp_name[1:]
        res = round(sum(data), self.lab.tags[tp_name].precision)
        return res

    def run(self):
        now = datetime.now().replace(microsecond=0)
        dt = now.replace(second=0,
                         minute=0,
                         hour=0,)
        tags = self.lab.tags

        if self.prev_day_stamp == 0:
            self.prev_day_stamp = dt.timestamp()
            self.prev_ftot = tags['ftot'].last_value
        prev_date = datetime.fromtimestamp(self.prev_day_stamp)
        if (now - prev_date).total_seconds() >= 60 * 60 * 24 + 60:
            it = prev_date + timedelta(days=1)
            while it < dt:
                pt = it

                logging.info(
                    ('Dayly: pt="{:%d.%m.%Y %H:%M:%S}" dt="{:%d.%m.%Y %H:%M:%S}" '
                     'prev_date="{:%d.%m.%Y %H:%M:%S}"').format(pt, dt, prev_date))

                # q_val = tags['ftot'].last_value - self.prev_ftot
                #
                # aph = round(self.avg_ph / self.count_ph if self.count_ph > 0 else tags['ph'].last_value,
                #             tags['ph'].precision)
                # af = round(self.avg_f / self.count_f if self.count_f > 0 else tags['f'].last_value,
                #            tags['f'].precision)
                # at = round(self.avg_t / self.count_t if self.count_t > 0 else tags['t'].last_value,
                #            tags['t'].precision)
                #

                q_val = self.tag_sum('ftot', pt)
                self.send_message('ftot', pt, q_val)

                if q_val < 0:
                    q_val = 0

                mvv = self.tag_sum('mvv', pt)
                mcl = self.tag_sum('mcl', pt)
                mfe = self.tag_sum('mfe', pt)
                moil = self.tag_sum('moil', pt)
                aph = self.tag_avg('ph', pt)
                af = self.tag_avg('f', pt)
                at = self.tag_avg('t', pt)
                avv = round(mvv / q_val if q_val > 0 else 0, tags['vv'].precision)
                a_oil = round(moil / q_val if q_val > 0 else 0, tags['oil'].precision)
                afe = round(mfe / q_val if q_val > 0 else 0, tags['fe'].precision)
                acl = round(mcl / q_val if q_val > 0 else 0, tags['cl'].precision)

                self.send_message('mvv', pt, mvv)
                self.send_message('mcl', pt, mcl)
                self.send_message('mfe', pt, mfe)
                self.send_message('moil', pt, moil)

                self.send_message('ph', pt, aph)
                self.send_message('f', pt, af)
                self.send_message('vv', pt, avv)
                self.send_message('oil', pt, a_oil)
                self.send_message('fe', pt, afe)
                self.send_message('cl', pt, acl)
                self.send_message('t', pt, at)

                self.prev_day_stamp = pt.timestamp()
                # self.prev_ftot = tags['ftot'].value
                #
                # self.avg_ph = \
                #     self.avg_f = \
                #     self.avg_t = \
                #     self.mvv = \
                #     self.moil = \
                #     self.mfe = \
                #     self.mcl = 0.0
                # self.count_ph = self.count_f = self.count_t = 0

                it += timedelta(days=1)

import logging
from datetime import datetime
from pystoki.utils import Configurable, TempState
from .task import Task, TimedStateMachine, TimedState
from ..tags import Tag
from ..hw import Device


class Waiting(TimedState):
    pass


class BarbIQ(TimedState):
    def on_enter(self, lab):
        lab.tags['f'].enabled = 0
        lab.tags['ftot'].enabled = 0
        lab.barbIQ_ctl(1)

    def on_leave(self, lab):
        lab.tags['f'].enabled = 1
        lab.tags['ftot'].enabled = 1
        lab.barbIQ_ctl(0)


class Revers(TimedState):
    revers_speed = Configurable('revers_speed', float, -50)
    old_speed = TempState('old_speed', float, 30)

    def on_enter(self, lab):
        if lab.freq_reg.speed != self.revers_speed:
            self.old_speed = lab.freq_reg.speed
        lab.freq_reg.set_speed(self.revers_speed)
        if hasattr(lab, 'clap_3hod'):
            lab.clap_3hod(1)

    def on_leave(self, lab):
        lab.freq_reg.set_speed(self.old_speed)


class Barb(TimedState):
    def on_enter(self, lab):
        logging.info('Barb: all flaps on')
        lab.barb1_ctl(1)
        lab.barb2_ctl(1)
        lab.barb3_ctl(1)

    def on_leave(self, lab):
        logging.info('Barb: all flaps off')
        lab.barb1_ctl(0)
        lab.barb2_ctl(0)
        lab.barb3_ctl(0)
        if hasattr(lab, 'clap_3hod'):
            lab.clap_3hod(0)


class Sliv(TimedState):
    def on_enter(self, lab):
        lab.sink_ctl(1)

    def on_leave(self, lab):
        lab.sink_ctl(0)


class Promivka2States(TimedStateMachine):
    prom_interval = Configurable('prom_interval', int, 12 * 60 * 60)
    revers_duration = Configurable('revers_duration', int, 30)
    barb_duration = Configurable('barb_duration', int, 30)
    sliv1_duration = Configurable('sliv1_duration', int, 60)
    nabor1_duration = Configurable('nabor1_duration', int, 5 * 50)
    sliv2_duration = Configurable('sliv2_duration', int, 60)
    nabor2_duration = Configurable('nabor2_duration', int, 15 * 60)

    old_lab_status = TempState('old_lab_status', int, 1)

    def __init__(self, code, lab):
        super().__init__(code, lab)
        # noinspection PyUnresolvedReferences
        tot_len = (self.revers_duration + self.barb_duration +
                   self.sliv1_duration + self.nabor1_duration +
                   self.sliv2_duration + self.nabor2_duration)
        # noinspection PyUnresolvedReferences
        waiting = Waiting(
            'waiting',
            self.prom_interval - tot_len,
            on_leave_f=self.set_status_washing,
            on_enter_f=self.set_status_normal,
            checks=[self.check_if_measuring]
        )
        revers = Revers('revers', self.revers_duration)
        barb = Barb('barb', self.barb_duration)
        sliv1 = Sliv('sliv1', self.sliv1_duration)
        nabor1 = Waiting('nabor1', self.nabor1_duration)
        sliv2 = Sliv('sliv2', self.sliv2_duration)
        nabor2 = Waiting('nabor2', self.nabor2_duration)

        waiting.next_state = revers
        revers.next_state = barb
        barb.next_state = sliv1
        sliv1.next_state = nabor1
        nabor1.next_state = sliv2
        sliv2.next_state = nabor2
        nabor2.next_state = waiting

        self.states = [
            waiting,
            revers,
            barb,
            sliv1,
            nabor1,
            sliv2,
            nabor2,
        ]

        # self.stopped_tags = ['t', 'ph', 'vv', 'oil', 'fe', 'cl', ]
        self.stopped_tags = ['t', 'ph', 'vv', 'oil', ]

        self._st = {}

    def set_status_washing(self, lab):
        # self.old_lab_status = lab.status
        lab.status = lab.STATUS_WASHING
        for t in self.stopped_tags:
            if t in lab.tags.keys():
                self._st[t] = lab.tags[t].enabled
                lab.tags[t].enabled = 0
        lab.dispatcher.send_message({
            'code': 200,
            'type': 'message',
            'date': datetime.now(),
            'value': 1,
            'quality': 0,
        })

    def set_status_normal(self, lab):
        lab.status = lab.STATUS_RUNNING
        logging.info(str(self.lab.tags))

        for t in self.stopped_tags:
            if t in lab.tags.keys():
                self.lab.tags[t].enabled = 1

        lab.dispatcher.send_message({
            'code': 200,
            'type': 'message',
            'date': datetime.now(),
            'value': 0,
            'quality': 0,
        })

    @staticmethod
    def check_if_measuring(lab):
        if 'fe' in lab.tags.keys():
            if lab.devices['micromac_fe'].status == Device.STATUS_BUSY:
                return False
        if 'cl' in lab.tags.keys():
            if lab.devices['micromac_cl'].status == Device.STATUS_BUSY:
                return False
        return True


class ProdIQStates(TimedStateMachine):
    barb_duration = Configurable('barb_duration', int, 60)
    barb_interval = Configurable('barb_interval', int, 12 * 60 * 60)

    def __init__(self, code, lab):
        super().__init__(code, lab)
        # noinspection PyUnresolvedReferences
        waiting = Waiting('waiting', self.barb_interval - self.barb_duration)
        barb = BarbIQ('barb', self.barb_duration)

        waiting.next_state = barb
        barb.next_state = waiting

        self.states = [
            waiting,
            barb,
        ]

    def run(self):
        if self.lab.status == self.lab.STATUS_WASHING:
            return

        super().run()


class ProdIQ(Task):
    timestamp = Configurable('timestamp', float, default=0.0)
    timestamp1 = Configurable('timestamp1', float, default=0.0)

    def __init__(self, code, lab):
        super().__init__(code, lab)
        self.running = False

    def run(self):
        dt = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - dt).total_seconds() >= 12 * 60 * 60 and self.lab.status != self.lab.STATUS_WASHING:
            self.lab.tags['f'].enabled = 0
            self.lab.tags['ftot'].enabled = 0
            if not self.running:
                logging.info('ProdIQ start')
                self.timestamp1 = datetime.now().timestamp()
                self.running = True
            # i7000_1 = self.lab.devices['i7000_1']
            # i7000_1.set_output(3,1)
            self.lab.barbIQ_ctl(1)
            d1 = datetime.fromtimestamp(self.timestamp1)
            if (datetime.now() - d1).total_seconds() >= 60:
                logging.info('ProdIQ stop')
                # i7000_1.set_output(3, 0)
                self.lab.barbIQ_ctl(0)
                self.lab.tags['f'].enabled = 1
                self.lab.tags['ftot'].enabled = 1
                self.timestamp = datetime.now().timestamp()
                self.running = False


class Promivka2(Task):
    STAGE_WAIT = 0
    STAGE_REVERS = 1
    STAGE_BARB = 2
    STAGE_SLIV1 = 3
    STAGE_NABOR1 = 4
    STAGE_SLIV2 = 5
    STAGE_NABOR2 = 6

    INTERVAL = 12 * 60 * 60
    REVERS_LEN = 30
    BARB_LEN = 60
    SLIV1_LEN = 60
    NABOR1_LEN = 5 * 60
    SLIV2_LEN = 60
    NABOR2_LEN = 10 * 60
    LEN = REVERS_LEN + BARB_LEN + SLIV1_LEN + NABOR1_LEN + SLIV2_LEN + NABOR2_LEN

    stage = Configurable('stage', int, default=0)
    timestamp = Configurable('timestamp', float, default=0.0)
    old_speed = Configurable('old_speed', int, default=30)

    def __init__(self, code, lab):
        super().__init__(code, lab)
        self.stages = [
            self.stage_wait,
            self.stage_revers,
            self.stage_barb,
            self.stage_sliv1,
            self.stage_nabor1,
            self.stage_sliv2,
            self.stage_nabor2,
        ]

        self.stopped_tags = [
            'vv',
            'oil',
            'ph',
            't',
        ]

    def stage_wait(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.INTERVAL - self.LEN:
            logging.info('Prom: revers')
            self.stage = self.STAGE_REVERS
            self.timestamp = datetime.now().timestamp()
            self.old_speed = self.lab.freq_reg.speed
            for t in self.stopped_tags:
                self.lab.tags[t].enabled = 0

    def stage_revers(self):
        self.lab.freq_reg.set_speed(-50)
        self.lab.clap_3hod(1)
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.REVERS_LEN:
            logging.info('Prom: barb')
            self.lab.freq_reg.set_speed(self.old_speed)
            self.stage = self.STAGE_BARB
            self.timestamp = datetime.now().timestamp()

    def stage_barb(self):
        self.lab.barb1_ctl(1)
        self.lab.barb2_ctl(1)
        self.lab.barb3_ctl(1)
        self.lab.clap_3hod(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.BARB_LEN:
            self.lab.barb1_ctl(0)
            self.lab.barb2_ctl(0)
            self.lab.barb3_ctl(0)
            self.lab.clap_3hod(0)
            logging.info('Prom: wait')
            self.stage = self.STAGE_SLIV1
            self.timestamp = datetime.now().timestamp()

    def stage_sliv1(self):
        self.lab.sink_ctl(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.SLIV1_LEN:
            logging.info('Prom: nabor1')
            self.lab.sink_ctl(0)
            self.stage = self.STAGE_NABOR1
            self.timestamp = datetime.now().timestamp()

    def stage_nabor1(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.NABOR1_LEN:
            logging.info('Prom: sliv2')
            self.stage = self.STAGE_SLIV2
            self.timestamp = datetime.now().timestamp()

    def stage_sliv2(self):
        self.lab.sink_ctl(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.SLIV2_LEN:
            logging.info('Prom: nabor2')
            self.lab.sink_ctl(0)
            self.stage = self.STAGE_NABOR2
            self.timestamp = datetime.now().timestamp()

    def stage_nabor2(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.NABOR2_LEN:
            logging.info('Prom: wait')
            self.stage = self.STAGE_WAIT
            self.timestamp = datetime.now().timestamp()
            for t in self.stopped_tags:
                self.lab.tags[t].enabled = 1

    def run(self):
        logging.info('Prom: enabled={}'.format(self.enabled))
        res = self.stages[self.stage]()

        if self.stage != self.STAGE_WAIT and self.lab.status != self.lab.STATUS_WASHING:
            self.lab._old_status = self.lab.status
            self.lab.status = self.lab.STATUS_WASHING

        if self.stage == self.STAGE_WAIT and self.lab.status == self.lab.STATUS_WASHING:
            self.lab.status = self.lab._old_status

        return res


class Promivka(Task):
    STAGE_WAIT = 0
    STAGE_REVERS = 1
    STAGE_BARB = 2
    STAGE_SLIV1 = 3
    STAGE_NABOR1 = 4
    STAGE_SLIV2 = 5
    STAGE_NABOR2 = 6

    INTERVAL = 12 * 60 * 60
    REVERS_LEN = 30
    BARB_LEN = 20
    SLIV1_LEN = 60
    NABOR1_LEN = 5 * 60
    SLIV2_LEN = 60
    NABOR2_LEN = 10 * 60
    LEN = REVERS_LEN + BARB_LEN + SLIV1_LEN + NABOR1_LEN + SLIV2_LEN + NABOR2_LEN

    stage = Configurable('stage', int, default=0)
    timestamp = Configurable('timestamp', float, default=0.0)
    old_speed = Configurable('old_speed', float, default=50)

    def __init__(self, code, lab):
        super().__init__(code, lab)
        self.stages = [
            self.stage_wait,
            self.stage_revers,
            self.stage_barb,
            self.stage_sliv1,
            self.stage_nabor1,
            self.stage_sliv2,
            self.stage_nabor2,
        ]

        self.stopped_tags = [
            'vv',
            'oil',
            'ph',
            't',
        ]

    def stage_wait(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.INTERVAL - self.LEN:
            logging.info('Prom: revers')
            self.stage = self.STAGE_REVERS
            self.timestamp = datetime.now().timestamp()
            self.old_speed = self.lab.freq_reg.speed
            for t in self.stopped_tags:
                self.lab.tags[t].enabled = 0

    def stage_revers(self):
        self.lab.freq_reg.set_speed(-100)
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.REVERS_LEN:
            logging.info('Prom: barb')
            self.lab.freq_reg.set_speed(self.old_speed)
            self.stage = self.STAGE_BARB
            self.timestamp = datetime.now().timestamp()

    def stage_barb(self):
        self.lab.barb_ctl(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.BARB_LEN:
            self.lab.barb_ctl(0)
            logging.info('Prom: sliv1')
            self.stage = self.STAGE_SLIV1
            self.timestamp = datetime.now().timestamp()

    def stage_sliv1(self):
        self.lab.sink_ctl(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.SLIV1_LEN:
            logging.info('Prom: nabor1')
            self.lab.sink_ctl(0)
            self.stage = self.STAGE_NABOR1
            self.timestamp = datetime.now().timestamp()

    def stage_nabor1(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.NABOR1_LEN:
            logging.info('Prom: sliv2')
            self.stage = self.STAGE_SLIV2
            self.timestamp = datetime.now().timestamp()

    def stage_sliv2(self):
        self.lab.sink_ctl(1)

        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.SLIV2_LEN:
            logging.info('Prom: nabor2')
            self.lab.sink_ctl(0)
            self.stage = self.STAGE_NABOR2
            self.timestamp = datetime.now().timestamp()

    def stage_nabor2(self):
        d = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - d).total_seconds() > self.NABOR2_LEN:
            logging.info('Prom: wait')
            self.stage = self.STAGE_WAIT
            self.timestamp = datetime.now().timestamp()
            for t in self.stopped_tags:
                self.lab.tags[t].enabled = 1

    def run(self):
        res = self.stages[self.stage]()

        if self.stage != self.STAGE_WAIT and self.lab.status != self.lab.STATUS_WASHING:
            self.lab._old_status = self.lab.status
            self.lab.status = self.lab.STATUS_WASHING

        if self.stage == self.STAGE_WAIT and self.lab.status == self.lab.STATUS_WASHING:
            self.lab.status = self.lab._old_status

        return res

from datetime import datetime
from pystoki.utils import TempState
from pystoki.tags import Tag
from .task import Task
import logging


# class CheckPump2(Task):
#     stamp = TempState('stamp', float, default=0)
#
#     def __init__(self, code, lab):
#         super().__init__(code, lab)
#
#     def run(self):
#         vega = self.lab.devices['vega']
#         prev = datetime.fromtimestamp(self.stamp)
#         if (datetime.now() - prev).total_seconds() > 60:
#             vega.set_speed(vega.speed)
#             self.stamp = datetime.now().timestamp()


class CheckPump(Task):
    stamp = TempState('stamp', float, default=0)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    def run(self):
        prev = datetime.fromtimestamp(self.stamp)
        if not hasattr(self.lab, 'freq_reg'):
            return
        if (datetime.now() - prev).total_seconds() > 60:
            # self.lab.freq_reg.run()
            if self.lab.freq_reg.speed > 0 and self.lab.freq_reg.cur_speed == 0:
                self.lab.freq_reg.run()
            self.stamp = datetime.now().timestamp()


class CheckPower(Task):

    pump_power = TempState('pump_power', int, default=0)
    gen = TempState('gen', int, default=0)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    def run(self):
        # i7000 = self.lab.devices['i7000_1']

        if self.lab.tags['pump_pow'].quality < Tag.QUALITY_BAD:
            p = int(self.lab.tags['pump_pow'].value)
            if p == 0:
                self.lab.pump_pow_ctl(1)

        if self.lab.tags['gen'].quality < Tag.QUALITY_BAD:
            g = int(self.lab.tags['gen'].value)
            if g == 0:
                self.lab.vent_lab_ctl(0)
                self.lab.vent_gen_ctl(0)
            else:
                self.lab.vent_lab_ctl(1)
                self.lab.vent_gen_ctl(1)

            # if g == 1 and self.gen == 0:
            #     self.lab.vent_lab_ctl(1)
            #     self.lab.vent_gen_ctl(1)
            #     # i7000.set_output(0, 1)
            #     # i7000.set_output(1, 1)
            #
            # if g == 0 and self.gen == 1:
            #     self.lab.vent_lab_ctl(0)
            #     self.lab.vent_gen_ctl(0)
            #     # i7000.set_output(0, 0)
            #     # i7000.set_output(1, 0)

            if self.gen != g:
                self.gen = g

        if self.lab.status != self.lab.STATUS_WASHING:
            if hasattr(self.lab, 'barb1_ctl'):
                self.lab.barb1_ctl(0)
                self.lab.barb2_ctl(0)
                self.lab.barb3_ctl(0)
                self.lab.clap_3hod(0)
                self.lab.sink_ctl(0)

        try:
            clap3_state = self.lab.tags['clap3_state']
            clap3_state.update()
            if self.lab.status != self.lab.STATUS_WASHING and clap3_state.value == 1:
                logging.info('Clap3 wrong state!')
                self.lab.clap_3hod(0)
        except KeyError:
            pass

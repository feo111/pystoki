import logging
from datetime import datetime
from .task import Task
from ..utils import TempState, Configurable
from ..hw import Device


class ValveSelect(Task):
    timestamp = TempState('timestamp', float, 0.0)
    zabor_change_timestamp = TempState('zabor_change_timestamp', float, 0.0)
    zabor = Configurable('zabor', str, 'lo')
    zabor_hi_min = Configurable('zabor_hi_min', float, 0.0)
    zabor_mid_min = Configurable('zabor_mid_min', float, 0.0)
    zabor_lo_min = Configurable('zabor_lo_min', float, 0.0)

    def __init__(self, code, lab):
        super().__init__(code, lab)

    def run(self):
        dt = datetime.fromtimestamp(self.timestamp)
        if (datetime.now() - dt).total_seconds() >= 60:
            vv, st = self.lab.devices['sc1000'].read_vv()
            if st == Device.STATUS_OK and self.lab.status == self.lab.STATUS_RUNNING:
                if vv > 100:
                    zabor_change_date = datetime.fromtimestamp(self.zabor_change_timestamp)
                    if (datetime.now() - zabor_change_date).total_seconds() > 60 * 10:
                        logging.info('Camera is empty!!!')
                        self.lab.dispatcher.send_message({
                            'code': 100,
                            'type': 'message',
                            'date': datetime.now(),
                            'value': 1,
                            'quality': 0,
                        })
                        if self.zabor == 'lo':
                            self.lab.valve_lo_ctl(0)
                            self.lab.valve_hi_ctl(1)
                            self.zabor = 'hi'
                            logging.info('Set zabor from mid')
                        elif self.zabor == 'mid':
                            self.lab.valve_mid_ctl(0)
                            self.lab.valve_lo_ctl(1)
                            self.zabor = 'lo'
                            logging.info('Set zabor from hi')
                        elif self.zabor == 'hi':
                            self.lab.valve_hi_ctl(0)
                            self.lab.valve_mid_ctl(1)
                            self.zabor = 'mid'
                            logging.info('Set zabor from lo')
                        self.zabor_change_timestamp = datetime.now().timestamp()
            if self.zabor == 'hi' and self.lab.tags['flev'].value < self.zabor_hi_min:
                self.lab.valve_hi_ctl(0)
                self.lab.valve_mid_ctl(1)
                self.zabor = 'mid'
                logging.info('Level less then min for hi. Set zabor from mid')
            if self.lab.tags['t'].quality < 2 and self.lab.tags['t'].value < 4:
                self.zabor = 'lo'
                logging.info('Temp less then 4. Set zabor from lo')
            fn_on = getattr(self.lab, 'valve_'+self.zabor + '_ctl')
            fn_on(1)
            self.timestamp = datetime.now().timestamp()

import json
import logging
import threading
import time
import urllib.error
import urllib.request
from queue import Queue

import os
import time

from .message import Message
from ..archive import Archive
from ..utils import json_datetime_handler, TEMP_DIR, check_date


class ServerError(Exception):
    pass


class ServerConnection(object):
    def __init__(self, server_url, proxy=None):
        self.server_url = server_url
        handlers = []
        if proxy is not None:
            handlers.append(urllib.request.ProxyHandler({'http': proxy}))
        self.opener = urllib.request.build_opener(*handlers)

    def send_request(self, sub_url, data):
        full_url = self.server_url + sub_url
        logging.debug('ServerConnection: Sending to {}: {}\n'.format(full_url, str(data)))
        data = data.encode('utf-8')

        request = urllib.request.Request(full_url,
                                         data,
                                         headers={'Content-Type': 'application/json'},

                                         )
        result = ''
        time_start = time.time()
        with self.opener.open(request, timeout=60) as resp:
            req_time = (time.time() - time_start) / 2.
            if resp.status != 200:
                raise ServerError('Bad server response from request "{}" data={}\n Server returns: {}'.format(
                    full_url,
                    data,
                    resp.status,
                ))
            server_date = resp.headers['Date']
            check_date(server_date, req_time)
            result = resp.read()

            logging.debug('ServerConnection: Receiving {}'.format(result))

        return result


class Dispatcher(object):

    def __init__(self, lab_code, server_url, proxy, messages_in_packet):
        self.lab_code = lab_code
        self._query = Queue()
        self._thread = None
        self.connection = ServerConnection(server_url, proxy)
        self.messages_in_packet = messages_in_packet
        self.load_saved()
        self._messages_listener = []
        self.archive = Archive()

    def load_saved(self):
        self._query = Queue()
        files = os.listdir(TEMP_DIR)
        files.sort()
        for f in files:
            file_name = os.path.join(TEMP_DIR, f)
            try:
                msg = Message(fn=file_name)
                self._query.put(msg)
            except ValueError:
                pass

    def register_listener(self, fn):
        self._messages_listener.append(fn)

    def unregister_listener(self, fn):
        self._messages_listener.remove(fn)

    def on_send_message(self, msg):
        for l in self._messages_listener:
            l(msg)

    def send_message(self, msg):
        m = Message(kwargs=msg)
        self.archive.add(m)
        self._query.put(m)
        self.on_send_message(m)

    def start(self):
        if self._thread is None:
            self._thread = threading.Thread(target=self._circle, daemon=True)
            self._thread.start()

    # noinspection PyTypeChecker
    def _circle(self):
        messages = None
        while True:
            # noinspection PyBroadException
            try:
                if messages is None:
                    messages = []
                    for i in range(self.messages_in_packet):
                        if self._query.empty():
                            break
                        messages.append(self._query.get())

                data = {
                    'UkmId': self.lab_code,
                    'values': [m.kwargs for m in messages],
                }
                to_send = json.dumps(data, default=json_datetime_handler)
                logging.info('Dispatcher: sending len' + str(len(to_send)))
                self.connection.send_request('/Data/AddCurrentData2/', to_send)
                for m in messages:
                    m.set_sent()
                messages = None

            except Exception:
                logging.exception('Dispatcher:')
            time.sleep(60)

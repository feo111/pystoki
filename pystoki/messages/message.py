import json
import logging
from datetime import datetime

import os
import os.path

from ..utils import TEMP_DIR, json_datetime_handler, json_datetime_hook


class Message(object):

    def __init__(self, fn='', kwargs=None, save=True):
        if kwargs is None:
            kwargs = {}
        self.file_name = None
        if fn == '':
            self.kwargs = kwargs
            if save:
                self.save()
        else:
            self.load(fn)
        self.sent = False

    def __getattr__(self, item):
        return self.kwargs[item]

    def __str__(self):
        return str(self.kwargs)

    def to_json(self):
        return json.dumps(self.kwargs, default=json_datetime_handler)

    @classmethod
    def from_json(cls, json_str):
        kwargs = json.loads(json_str, object_hook=json_datetime_hook)
        return cls(kwargs=kwargs, save=False)

    @classmethod
    def get_fn(cls):
        fn = os.path.join(TEMP_DIR, '{:025.06f}'.format(datetime.now().timestamp()).replace('.', '_'))
        if not os.path.exists(fn):
            return fn
        for i in range(100000):
            fn1 = '{}_{:05}'.format(fn, i)
            if not os.path.exists(fn1):
                return fn1

    def save(self):
        fn = self.get_fn()
        logging.debug('Message save: file_name={}'.format(fn))
        with open(fn, 'w') as f:
            json.dump(self.kwargs, f, default=json_datetime_handler)
        self.file_name = fn

    def load(self, fn):
        logging.debug('Message load: file_name={}'.format(fn))
        with open(fn) as f:
            self.kwargs = json.load(f, object_hook=json_datetime_hook)
        logging.debug('Loaded message: file={} data={}'.format(fn, self.kwargs))
        self.file_name = fn

    def set_sent(self):
        self.sent = True
        if self.file_name is not None:
            logging.debug('Message: removing file {}'.format(self.file_name))
            if os.path.exists(self.file_name):
                os.remove(self.file_name)
            self.file_name = None




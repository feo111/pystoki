import logging
from datetime import datetime

from ..hw import Device, ProtoError
from ..lab import Laboratory
from ..utils import Configurable, TempState


# noinspection PyTypeChecker
class Tag(object):
    QUALITY_GOOD = 0
    QUALITY_WARNING = 1
    QUALITY_ERROR = 2
    QUALITY_BAD = 3
    QUALITY_DISABLED = 4
    QUALITY_MEASURING = 5
    QUALITY_INIT = 6

    code = ''
    value = 0
    quality = QUALITY_INIT
    date = datetime(1900, 1, 1)

    min_val = Configurable('min_val', float, 0)
    max_val = Configurable('max_val', float, 2 ** 32)
    min_error = Configurable('min_error', float, 0)
    max_error = Configurable('max_error', float, 2 ** 32)
    interval = Configurable('interval', int, 60)
    enabled = Configurable('enabled', int, 1)
    precision = Configurable('precision', int, 2)
    min_val_msg_id = Configurable('min_val_msg_id', int, 0)
    max_val_msg_id = Configurable('max_val_msg_id', int, 0)

    last_value = TempState('last_value', float, 0)
    last_date = TempState('last_date', float, 0)
    prev_value = TempState('prev_value', float, 0)
    prev_date = TempState('prev_date', float, 0)
    min_msg_active = TempState('min_msg_active', int, 0)
    max_msg_active = TempState('max_msg_active', int, 0)
    errors_count = TempState('errors_count', int, 0)

    def __init__(self, code, fn=None, on_change=False, use_filter=False):
        self.code = code
        self.fn = fn
        self.on_change = on_change
        self.value = self.last_value
        self.date = datetime.fromtimestamp(self.last_date)
        self.changed = False
        self._update_listeners = []
        self.use_filter = use_filter
        if self.prev_date == 0:
            self.prev_value = self.last_value
            self.prev_date = self.last_date

    def register_update_listener(self, fn):
        self._update_listeners.append(fn)

    def unregister_update_listener(self, fn):
        self._update_listeners.remove(fn)

    def on_update(self):
        for fn in self._update_listeners:
            fn(self)

    def send(self):
        logging.info('Tag send: {}, val={}, status={}'.format(self.code, self.value, self.quality))
        msg = {
            'code': self.code,
            'type': 'current',
            'date': self.date,
            'value': self.value,
            'quality': self.quality,
        }
        Laboratory.get_lab().dispatcher.send_message(msg)

    def send_warn_msg(self, msg_id, active):
        logging.warning('Tag warning: {} msg={} state={}'.format(self.code, msg_id, active))
        msg = {
            'code': str(msg_id),
            'type': 'message',
            'date': self.date,
            'value': active,
            'quality': self.quality,
        }
        Laboratory.get_lab().dispatcher.send_message(msg)

    def update(self):
        """
        :return:
         True - измерение закончено - следующее по интервалу
         False - измерение в процессе
        """
        old_value = self.value
        old_status = self.quality
        init = (self.quality == self.QUALITY_INIT or
                (datetime.now().minute == 0 and datetime.now().second < 5 and self.on_change))

        # Если нет функции - ничего не делаем
        if self.fn is None:
            return True

        # Если тэг выключен - ничего не делать
        # if self.quality == Tag.QUALITY_DISABLED:
        #     return True
        if self.enabled == 0:
            if self.value != 0 and self.quality != Tag.QUALITY_DISABLED:
                self.value = 0
                self.quality = Tag.QUALITY_DISABLED
                self.send()
            return True

        # Записать время начала измерения
        if self.quality != Tag.QUALITY_MEASURING:
            self.date = datetime.now().replace(second=0,
                                               microsecond=0,
                                               )

        # Выполняем измерение
        try:
            val, status = self.fn()
            self.errors_count = 0
            self.quality = Tag.QUALITY_GOOD
        except ProtoError:
            if self.errors_count < 3:
                self.errors_count += 1
                val = old_value
                status = self.quality = Tag.QUALITY_ERROR
            else:
                val = 0
                status = self.quality = Tag.QUALITY_BAD
            if old_status == Tag.QUALITY_MEASURING:
                status = self.quality = Tag.QUALITY_MEASURING

        # Если измерение длительное - обратиться позже
        if status == Device.STATUS_BUSY:
            self.quality = Tag.QUALITY_MEASURING
            return False

        if self.quality == Tag.QUALITY_GOOD:
            if val < self.min_error or val > self.max_error:
                self.quality = Tag.QUALITY_ERROR

        # Выдача сообщений
        # self.apply_filter()
        # val = round(val, self.precision)
        self.value = round(self.filter_func(val), self.precision)

        if self.quality == Tag.QUALITY_GOOD:
            if self.value < self.min_error or self.value > self.max_error:
                self.quality = Tag.QUALITY_ERROR

        if self.quality == Tag.QUALITY_GOOD:
            if val < self.min_val or val > self.max_val:
                self.quality = Tag.QUALITY_WARNING
            if val < self.min_val and self.min_msg_active == 0 and self.min_val_msg_id != 0:
                self.min_msg_active = 1
                self.send_warn_msg(self.min_val_msg_id, 1)
            if val >= self.min_val and self.min_msg_active == 1 and self.min_val_msg_id != 0:
                self.min_msg_active = 0
                self.send_warn_msg(self.min_val_msg_id, 0)
            if val > self.max_val and self.max_msg_active == 0 and self.max_val_msg_id != 0:
                self.max_msg_active = 1
                self.send_warn_msg(self.max_val_msg_id, 1)
            if val <= self.max_val and self.max_msg_active == 1 and self.max_val_msg_id != 0:
                self.max_msg_active = 0
                self.send_warn_msg(self.max_val_msg_id, 0)

        self.prev_value = self.last_value
        self.prev_date = self.last_date
        if self.quality <= Tag.QUALITY_WARNING:
            self.last_value = self.value
            self.last_date = self.date.timestamp()
        else:
            self.last_value = 0
            self.last_date = self.date.timestamp()

        # Отправим сообщение о изменении тэга
        if self.on_change and not init:
            if old_value != self.value:
                self.send()
        else:
            self.send()

        self.on_update()
        return True

    # def apply_filter(self):
    #     if not self.use_filter:
    #         return
    #     if self.quality >= Tag.QUALITY_ERROR:
    #         return
    #     self.value = self.filter_func(self.value)

    def filter_func(self, val):
        return val


class FilteredTag(Tag):
    window = Configurable('window', int, 1)
    prev_val = TempState('prev_val', float, 0)
    cur_n = TempState('cur_n', int, 0)

    # noinspection PyTypeChecker,PyTypeChecker
    def filter_func(self, val):
        # if self.cur_n == 0:
        #     self.cur_n = 1
        #     self.prev_val = val
        #     return val
        p = self.prev_val
        n = self.cur_n
        res = (self.prev_val * self.cur_n + val) / (self.cur_n + 1)
        self.prev_val = res

        if self.cur_n < self.window:
            self.cur_n += 1
        logging.info('Filter: tag={} input={} prev={} n={} res={}'.format(self.code, val, p, n, res))
        return res

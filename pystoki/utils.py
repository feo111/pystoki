import logging
from configparser import ConfigParser
from datetime import datetime, timedelta
import functools

import os
import os.path
import pytz
import tzlocal
import math
import subprocess

BASE_DIR = '/var/lib/stoki/'
ARCHIVE_DIR = os.path.join(BASE_DIR, 'archive')
LOG_FILE = os.path.join(BASE_DIR, 'stoki.log')
TEMP_FILE = os.path.join(BASE_DIR, 'stoki.tmp')
TEMP_DIR = os.path.join(BASE_DIR, 'tmp')
CONFIG_FILE = os.path.join(BASE_DIR, 'stoki.cfg')

DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


def logging_init():
    logging.basicConfig(filename=LOG_FILE, filemode='a', level=logging.INFO, format='%(asctime)s %(message)s')


def create_dirs():
    for d in (BASE_DIR, ARCHIVE_DIR, TEMP_DIR):
        if not os.path.exists(d):
            os.mkdir(d, 0o775)


class State(object):
    """
    Дескриптор для сохранения состояния аттрибута между запусками программы

    Потомок должен переоределить :source_file: или вызвать State.set_file_name перед создание объекта

    >>> import os
    >>> from datetime import datetime
    >>> class TestState(State):
    ...     source_file = '/tmp/test_state.cfg'
    >>> class TestClass(object):
    ...     test_var = TestState('test_var')
    ...     def __init__(self):
    ...         self.code = "code1"
    >>> a = TestClass()
    >>> a.test_var
    ''
    >>> a.test_var = "Hello"
    >>> a = None
    >>> b = TestClass()
    >>> b.test_var
    'Hello'
    >>> class TestClass1(object):
    ...     test_date = TestState('test_date', datetime)
    ...     code = 'code2'
    >>> c = TestClass1()
    >>> c.test_date == datetime.min
    True
    >>> c.test_date = datetime(year=2016, month=1, day=3)
    >>> c = None
    >>> d = TestClass1()
    >>> d.test_date
    datetime.datetime(2016, 1, 3, 0, 0)
    >>> os.remove('/tmp/test_state.cfg')
    """
    source = None
    source_file = None

    def __init__(self, name, type_=None, default=None):
        if self.source_file is None:
            raise TypeError('type {} is not instantiable'.format(type_(self)))

        if self.source is None:
            self.load_config()

        if type_ is None:
            type_ = str
            default = ''
        if type_ is datetime:
            default = str(datetime.min.timestamp())

        self.name = name
        self.type = type_

        self.default = default

    # noinspection PyUnusedLocal
    def __get__(self, instance, owner):
        if instance is None:
            return self
        val = self.source.get(instance.code, self.name, fallback=self.default)
        if self.type is datetime:
            res = datetime.fromtimestamp(float(val))
        else:
            res = self.type(val)
        return res

    def __set__(self, instance, value):
        if instance is None:
            raise AttributeError("Can't set attribute")
        if not self.source.has_section(instance.code):
            self.source.add_section(instance.code)

        v = value
        if self.type is datetime:
            v = value.timestamp()
        self.source.set(instance.code, self.name, str(v))
        with open(self.source_file + '.tmp', 'w') as f:
            self.source.write(f)
        os.rename(self.source_file + '.tmp', self.source_file)

    def __delete__(self, instance):
        raise AttributeError("Can't delete attribute")

    @classmethod
    def set_file_name(cls, fn):
        cls.source_file = fn

    @classmethod
    def load_config(cls):
        cls.source = ConfigParser()
        if os.path.exists(cls.source_file):
            cls.source.read(cls.source_file)


class Configurable(State):
    source_file = CONFIG_FILE


class TempState(State):
    source_file = TEMP_FILE


def json_datetime_hook(dct):
    for k, v in dct.items():
        if isinstance(v, str):
            try:
                dct[k] = datetime.strptime(v, DATE_FORMAT)
            except ValueError:
                pass
    return dct


def json_datetime_handler(obj):
    if hasattr(obj, 'timestamp'):
        return obj.isoformat()
    else:
        raise TypeError('Object of type {} with value {} is not JSON serializable'.format(type(obj), repr(obj)))


def repeatedly(n=1, ex_cls=None):
    """
    Декоратор, делает несколько попыток вызова функции
    """
    if ex_cls is None:
        ex_cls = Exception
    if not issubclass(ex_cls, Exception):
        raise TypeError('ex_cls must be subtype of Exception. Given {}'.format(ex_cls))

    def wrap(f):
        @functools.wraps(f)
        def wrapped_f(*args, **argv):
            i = 0
            ok = False
            res = None
            while not ok:
                try:
                    res = f(*args, **argv)
                    ok = True
                except ex_cls:
                    i += 1
                    if i >= n:
                        raise
                        # else:
                        #   logging.exception('Error in function {}. Try={}'.format(f, i))
            return res

        return wrapped_f

    return wrap


def check_archive(f=None, t=None, verbose=False):
    """
    Проверка архивов
    :param verbose: Вывод в stdout
    :param f: Дата с
    :param t: Дата по
    :return: True если все нормально, False если ошибки
    """
    if f is None:
        f = datetime(2017, 1, 1)
    if t is None:
        t = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    if type(f) is not datetime or type(t) is not datetime:
        raise TypeError('Arguments must be datetime')
    from .archive import Archive
    arc = Archive()
    day = t
    tag_name = 'ftot'
    ok = True
    while day > f:
        day -= timedelta(days=1)
        data1 = arc.read(day, day.replace(hour=23, minute=59, second=59), 'archive_hourly', tag_name)
        s1 = sum([float(d.value) for d in data1])
        data = arc.read(day, day + timedelta(days=1, minutes=1), 'current', tag_name)
        s2 = float(data[-1].value) - float(data[0].value)
        if verbose:
            print(day, s1, s2, s1 == s2)
        if s1 != s2:
            ok = False
    return ok


def check_date(server_date=None, request_time=0.):
    if server_date is None:
        return
    dt = pytz.utc.localize(datetime.strptime(server_date, '%a, %d %b %Y %H:%M:%S %Z'))
    dl = dt.astimezone(tzlocal.get_localzone())
    delta = (datetime.now(tzlocal.get_localzone()) - dl).total_seconds() - request_time
    logging.info('Check_date: delta={}'.format(delta))
    if abs(delta) > 10.:
        try:
            res = subprocess.call(['date', '-s', dl.strftime('%Y-%m-%d %H:%M:%S')], timeout=5)
            logging.info('SetDate: date -s returns {}'.format(res))
            res = subprocess.call(['hwclock', '--systohc'], timeout=5)
            logging.info('SetDate: hwclock --systohc returns {}'.format(res))
        except subprocess.TimeoutExpired:
            logging.error('SetDate: timeout')
